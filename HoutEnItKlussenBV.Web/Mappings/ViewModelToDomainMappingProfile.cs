﻿namespace HoutEnItKlussenBV.Web.Mappings
{
    using AutoMapper;
    using Entities;
    using Models;

    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            //Mapper.CreateMap<TaskViewModel, Task>()
            //   //.ForMember(m => m.Image, map => map.Ignore())
            //   .ForMember(m => m.Part, map => map.Ignore());

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TaskViewModel, Task>()
                //    .ForMember(destination => destination., map => map.Ignore())
                ;
            });
            config.AssertConfigurationIsValid();
        }
    }
}
