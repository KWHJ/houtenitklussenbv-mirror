﻿namespace HoutEnItKlussenBV.Web.Mappings
{
    using System.Linq;
    using AutoMapper;
    using Entities;
    using Models;


    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        /// <summary>
        /// Protected override void Configure
        /// </summary>
        protected override void Configure()
        {
            ////--TEST
            //var config = new MapperConfiguration(cfg =>
            //{
            //    #region "Mapping Task -> TaskViewModel"
            //    cfg.CreateMap<Task, TaskViewModel>()
            //        .ForMember(vm => vm.Part, map => map.MapFrom(
            //            m => m.Part.Name))
            //        .ForMember(vm => vm.PartId, map => map.MapFrom(
            //            m => m.Part.ID))
            //        .ForMember(vm => vm.IsAvailable, map => map.MapFrom(
            //            m => m.Stocks.Any(s => s.IsAvailable)))
            //        .ForMember(vm => vm.NumberOfStocks, map => map.MapFrom(
            //            m => m.Stocks.Count))
            //        .ForMember(vm => vm.Image, map => map.MapFrom(
            //            m => string.IsNullOrEmpty(m.Image) == true ? "unknown.jpg" : m.Image));
            //    #endregion

            //    #region "Mapping Part -> PartViewModel"
            //    cfg.CreateMap<Part, PartViewModel>()
            //        .ForMember(vm => vm.NumberOfTasks, map => map.MapFrom(
            //            mTEST => mTEST.Tasks.Count()));
            //    #endregion

            //    cfg.CreateMap<Customer, CustomerViewModel>();
            //    cfg.CreateMap<Stock, StockViewModel>();
            //    cfg.CreateMap<Rental, RentalViewModel>();
            //});
            //config.AssertConfigurationIsValid();
            ////--TEST

            Mapper.CreateMap<Task, TaskViewModel>()
                .ForMember(vm => vm.Id, map => map.MapFrom(m => m.Id))
                .ForMember(vm => vm.Name, map => map.MapFrom(m => m.Name))
                .ForMember(vm => vm.ImageNames, map => map.MapFrom(m => m.ImageNames))
                //.ForMember(vm => vm.TaskImages, map => map.MapFrom(m => m.TaskImages))
                ;

            //Mapper.CreateMap<Customer, CustomerViewModel>();

            //Mapper.CreateMap<User, UserViewModel>();

            //Mapper.CreateMap<Stock, StockViewModel>();


        }
    }
}
