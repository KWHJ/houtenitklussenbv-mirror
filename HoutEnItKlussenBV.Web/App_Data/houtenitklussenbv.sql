/****** Database [HoutEnItKlussenBV] ******/
USE master
IF EXISTS(SELECT * FROM master.dbo.sysdatabases WHERE [Name] = N'HoutEnItKlussenBV')
DROP DATABASE HoutEnItKlussenBV
GO
/*
If message "Cannot drop database because it is currently in use." is returned, while using Master you need to close SQL Server Management Studio completely
and again re-open it and connect to it and re-run database creation script
*/
CREATE DATABASE HoutEnItKlussenBV
GO

USE HoutEnItKlussenBV
GO
/*
Sets certain database behaviors to be compatible with the specified version of SQL Server.
Product				Database Engine Version		Compatibility Level Designation		Supported Compatibility Level Values
SQL Server 2012		11							110									110, 100, 90
*/
ALTER DATABASE [HoutEnItKlussenBV] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HoutEnItKlussenBV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
/*
Gets or sets a Boolean property value that specifies whether the ANSI_NULL_DEFAULT database option is active
If True, user-defined data types default to allowing NULL values. Otherwise, False (default).
Remarks:
•This option lets the user control the database default nullability at a database level.
•This option can also be changed by using the Transact-SQL ALTER DATABASE statement.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET ANSI_NULL_DEFAULT OFF
GO

/*
When SET ANSI_NULLS is OFF, the Equals (=) and Not Equal To (<>) comparison operators do not follow the ISO standard.
A SELECT statement that uses WHERE column_name = NULL returns the rows that have null values in column_name.
A SELECT statement that uses WHERE column_name <> NULL returns the rows that have nonnull values in the column.
Also, a SELECT statement that uses WHERE column_name <> XYZ_value returns all rows that are not XYZ_value and that are not NULL.
When SET ANSI_NULLS is ON, a SELECT statement that uses WHERE column_name = NULL returns zero rows
even if there are null values in column_name.
A SELECT statement that uses WHERE column_name <> NULL returns zero rows even if there are nonnull values in column_name.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET ANSI_NULLS OFF
GO

/*
Controls the way the column stores values shorter than the defined size of the column,
and the way the column stores values that have trailing blanks in char, varchar, binary, and varbinary data.

Remarks
•Columns defined with char, varchar, binary, and varbinary data types have a defined size.
This setting affects only the definition of new columns.
•After the column is created, SQL Server stores the values based on the setting when the column was created.
Existing columns are not affected by a later change to this setting.

*/
ALTER DATABASE [HoutEnItKlussenBV] SET ANSI_PADDING OFF
GO

/*
Specifies ISO standard behavior for several error conditions.

Remarks
SET ANSI_WARNINGS affects the following conditions:
•When set to ON, if null values appear in aggregate functions,
such as SUM, AVG, MAX, MIN, STDEV, STDEVP, VAR, VARP, or COUNT,
a warning message is generated. When set to OFF, no warning is issued.
•When set to ON, the divide-by-zero and arithmetic overflow errors
cause the statement to be rolled back and an error message is generated.
•When set to OFF, the divide-by-zero and arithmetic overflow errors
cause null values to be returned.
The behavior in which a divide-by-zero or arithmetic overflow error
causes null values to be returned occurs if an INSERT or UPDATE is tried
on a character, Unicode, or binary column in which the length of a new value exceeds the maximum size of the column.
If SET ANSI_WARNINGS is ON, the INSERT or UPDATE is canceled as specified by the ISO standard.
Trailing blanks are ignored for character columns and trailing nulls are ignored for binary columns.
When OFF, data is truncated to the size of the column and the statement succeeds
*/
ALTER DATABASE [HoutEnItKlussenBV] SET ANSI_WARNINGS OFF
GO

/*
Terminates a query when an overflow or divide-by-zero error occurs during query execution.
Remarks
•You should always set ARITHABORT to ON in your logon sessions.
Setting ARITHABORT to OFF can negatively impact query optimization leading to performance issues.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET ARITHABORT ON
GO

/*
This rule checks whether the AUTO_ CLOSE option is set OFF.
When AUTO_CLOSE is set ON, this option can cause performance degradation on frequently accessed databases
because of the increased overhead of opening and closing the database after each connection.
AUTO_CLOSE also flushes the procedure cache after each connection.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET AUTO_CLOSE OFF
GO

/*
This rule checks whether the AUTO_SHRINK database option is set to OFF.
Frequently shrinking and expanding a database can lead to physical fragmentation.
Best Practices Recommendations
Set the AUTO_SHRINK database option to OFF.
If you know that the space that you are reclaiming will not be needed in the future,
you can reclaim the space by manually shrinking the database.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET AUTO_SHRINK OFF
GO

/*
Displays or changes the automatic statistics update option, AUTO_UPDATE_STATISTICS, for an index,
a statistics object, a table, or an indexed view.
Remarks
•If the specified index is disabled, or the specified table has a disabled clustered index, an error message is displayed.
AUTO_UPDATE_STATISTICS is always OFF for memory-optimized tables

*/
ALTER DATABASE [HoutEnItKlussenBV] SET AUTO_UPDATE_STATISTICS ON
GO

/*
Controls the behavior of the Transact-SQL COMMIT TRANSACTION statement.
The default value for this setting is OFF. This means that the server will not close cursors when you commit a transaction.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

/*
This property makes each cursor local to the object that declared it when set to local.
When it is set to 'Global' (default), the scope of the cursor can be extended outside the object that created it
*/
ALTER DATABASE [HoutEnItKlussenBV] SET CURSOR_DEFAULT GLOBAL
GO

/*
Controls whether concatenation results are treated as null or empty string values.
Remarks
•When SET CONCAT_NULL_YIELDS_NULL is ON, concatenating a null value with a string yields a NULL result.
For example, SELECT 'abc' + NULL yields NULL.
•When SET CONCAT_NULL_YIELDS_NULL is OFF, concatenating a null value with a string yields the string itself
(the null value is treated as an empty string). For example, SELECT 'abc' + NULL yields abc.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET CONCAT_NULL_YIELDS_NULL OFF
GO

/*
Specifies the level of error reporting generated when rounding in an expression causes a loss of precision.
Remarks
•When SET NUMERIC_ROUNDABORT is ON, an error is generated after a loss of precision occurs in an expression.
If SET NUMERIC_ROUNDABORT is ON, SET ARITHABORT determines the severity of the generated error.
•When OFF, losses of precision do not generate error messages and the result is rounded to the
precision of the column or variable storing the result.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET NUMERIC_ROUNDABORT ON
GO

/*
Causes SQL Server to follow the ISO rules regarding quotation mark delimiting identifiers and literal strings.
Identifiers delimited by double quotation marks can be either Transact-SQL reserved keywords or can
contain characters not generally allowed by the Transact-SQL syntax rules for identifiers.
Remarks
•When SET QUOTED_IDENTIFIER is ON, identifiers can be delimited by double quotation marks,
and literals must be delimited by single quotation marks.
•When SET QUOTED_IDENTIFIER is OFF, identifiers cannot be quoted
and must follow all Transact-SQL rules for identifiers.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET QUOTED_IDENTIFIER OFF
GO

/*
This topic describes how to configure the nested triggers server configuration option in SQL Server 2016 by
using SQL Server Management Studio or Transact-SQL.
The nested triggers option controls whether an AFTER trigger can cascade. That is, perform an action that
initiates another trigger, which initiates another trigger, and so on.
When nested triggers is set to 0, AFTER triggers cannot cascade.
When nested triggers is set to 1 (the default), AFTER triggers can cascade to as many as 32 levels.
INSTEAD OF triggers can be nested regardless of the setting of this option.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET RECURSIVE_TRIGGERS OFF
GO

/*
SET ENABLE_BROKER command needs an exclusive lock on the database.
This statement completes immediately unless there are open connections to the database that have
locked resources in the database.
ENABLE_BROKER will wait until the other sessions release their locks.
To enable Service Broker in the msdb database, you will need to stop SQL Server Agent to release the locks.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET ENABLE_BROKER
GO

/*
The AUTO_UPDATE_STATISTICS_ASYNC option affects how automatic statistics updates are applied to your SQL Server database.
This option is set database by database and the default setting for this option is disabled.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

/*
The DATE_CORRELATION_OPTIMIZATION setting controls whether SQL Server maintains correlation statistics
between any two tables that are related using single-column FOREIGN KEY constraints and have
columns using the DATETIME datatype. The default value is OFF.
Changing the value to ON can speedup queries that join two related tables and filter the output
using a WHERE clause which includes a column with DATETIME datatype.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET DATE_CORRELATION_OPTIMIZATION ON
GO

/*
The TRUSTWORTHY database property is used to indicate whether the instance of SQL Server trusts the database
and the contents within it. By default, this setting is OFF, but can be set to ON by using the ALTER DATABASE statement.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET TRUSTWORTHY OFF
GO

/*
https://msdn.microsoft.com/en-us/library/tcbchxcb(v=vs.110).aspx
*/
ALTER DATABASE [HoutEnItKlussenBV] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

/*
When the PARAMETERIZATION database option is set to SIMPLE, the SQL Server query optimizer may choose to parameterize the queries.
This means that any literal values that are contained in a query are substituted with parameters.
This process is referred to as simple parameterization.
When SIMPLE parameterization is in effect, you cannot control which queries are parameterized and which queries are not.
However, you can specify that all queries in a database be parameterized by setting the PARAMETERIZATION database option to FORCED.
This process is referred to as forced parameterization.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET PARAMETERIZATION SIMPLE
GO

/*
https://msdn.microsoft.com/en-us/library/tcbchxcb(v=vs.110).aspx
READ COMMITTED is the default isolation level for SQL Server.
It prevents dirty reads by specifying that statements cannot read data values that have been modified
but not yet committed by other transactions. Other transactions can still modify, insert, or delete data
between executions of individual statements within the
current transaction, resulting in non-repeatable reads, or "phantom" data.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET READ_COMMITTED_SNAPSHOT ON
GO

/*
https://msdn.microsoft.com/en-us/library/bb934170.aspx
*/
ALTER DATABASE [HoutEnItKlussenBV] SET HONOR_BROKER_PRIORITY OFF
GO

/*
SQL Server backup and restore operations occur within the context of the recovery model of the database.
Recovery models are designed to control transaction log maintenance.
A recovery model is a database property that controls how transactions are logged, whether the transaction log
requires (and allows) backing up, and what kinds of restore operations are available.
Three recovery models exist: simple, full, and bulk-logged.
Typically, a database uses the full recovery model or simple recovery model.
A database can be switched to another recovery model at any time.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET RECOVERY SIMPLE
GO

/*
This topic describes how to set a user-defined database to single-user mode in SQL Server 2016
by using SQL Server Management Studio or Transact-SQL.
Single-user mode specifies that only one user at a time can access the database
and is generally used for maintenance actions.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET MULTI_USER
GO

/*
This rule checks whether PAGE_VERIFY database option is set to CHECKSUM.
When CHECKSUM is enabled for the PAGE_VERIFY database option, the SQL Server Database Engine
calculates a checksum over the contents of the whole page, and stores the value in the page header
when a page is written to disk.
When the page is read from disk, the checksum is recomputed and compared to the checksum value
that is stored in the page header.
This helps provide a high level of data-file integrity.
*/
ALTER DATABASE [HoutEnItKlussenBV] SET PAGE_VERIFY CHECKSUM
GO

/*
Cross-database ownership chaining occurs when a procedure in one database depends on objects in another database.
A cross-database ownership chain works in the same way as ownership chaining within a single database,
except that an unbroken ownership chain requires that all the object owners are mapped to the same login account.
If the source object in the source database and the target objects in the target databases are owned
by the same login account, SQL Server does not check permissions on the target objects.
Default of OFF
*/
ALTER DATABASE [HoutEnItKlussenBV] SET DB_CHAINING OFF
GO

/*
https://msdn.microsoft.com/en-us/library/gg509097.aspx
*/
ALTER DATABASE [HoutEnItKlussenBV] SET FILESTREAM ( NON_TRANSACTED_ACCESS = OFF )
GO

/*
--This topic describes how to set the change the target recovery time of a SQL Server database in SQL Server 2016
--by using SQL Server Management Studio or Transact-SQL.
--By default, the target recovery time is 0, and the database uses automatic checkpoints (which are controlled
--by the recovery interval server option).
--Setting the target recovery time to greater than 0 causes the database to use the indirect-checkpoints
--and establishes an upper-bound on recovery time for this database.
--*/
--ALTER DATABASE [HoutEnItKlussenBV] SET TARGET_RECOVERY_TIME = 0 SECONDS
--GO

USE [HoutEnItKlussenBV]
GO
/****** Table [dbo].[__MigrationHistory] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory]
(
	[MigrationId]		nvarchar(150)	NOT NULL,
	[ContextKey]		nvarchar(300) NOT NULL,
	[Model]				varbinary(max) NOT NULL,
	[ProductVersion]	nvarchar(32) NOT NULL,
	CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED ( [MigrationId] ASC, [ContextKey] ASC )
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
	TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Table [dbo].[Error] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Error](
	[Id]					int identity(1,1)	NOT NULL,
	[Message]				nvarchar(max)		NULL,
	[StackTrace]			nvarchar(max)		NULL,
	[DateCreated]			datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Error] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON )
	) ON [PRIMARY]
	TEXTIMAGE_ON [PRIMARY]
GO

/****** Table [dbo].[Customer] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer]
(
--[Id], [FirstName], [LastName], [Email], [UniqueKey], [DateOfBirth], [Mobile], [Phone], [RegistrationDate], [AddressId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[FirstName]				nvarchar(100)		NOT NULL,
	[LastName]				nvarchar(100)		NOT NULL,
	[Email]					nvarchar(200)		NOT NULL,
	[UniqueKey]				uniqueidentifier	NOT NULL,
	[DateOfBirth]			datetime			NOT NULL,
	[Mobile]				nvarchar(20)		NOT NULL,
	[Phone]					nvarchar(20)		NULL,
	[RegistrationDate]		datetime			NOT NULL,
	[AddressId]				int					NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Customer] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[Address] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address]
(
--[Id], [Street], [Number], [NumberExt], [Zipcode], [Town], [Province], [Country], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1, 1)	NOT NULL,
	[Street]				nvarchar(100)		NOT NULL,
	[Number]				nvarchar(20)		NOT NULL,
	[NumberExt]				nvarchar(20)		NULL,
	[Zipcode]				nvarchar(20)		NOT NULL,
	[Town]					nvarchar(50)		NOT NULL,
	[Province]				nvarchar(100)		NOT NULL,
	[Country]				nvarchar(100)		NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Address] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

--/****** Table [dbo].[Project] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project]
(
--[Id], [Name], [Description], [CustomerId], [DateCreated], [DateReady], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1, 1)	NOT NULL,
	[Name]					nvarchar(100)		NOT NULL,
	[Description]			nvarchar(max)		NOT NULL,
	[CustomerId]			int					NOT NULL,
	[DateCreated]			datetime			NOT NULL,
	[DateReady]				datetime			NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Project] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[ProjectTask] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectTask]
(
--[Id], [ProjectId], [TaskId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[ProjectId]				int					NOT NULL,
	[TaskId]				int					NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.ProjectTask] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[Task] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Task]
(
--[Id], [Name], [Description], [Image], [TaskTypeId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[Name]					nvarchar(100)		NOT NULL,
	[Description]			nvarchar(max)		NOT NULL,
	[Image]					nvarchar(100)		NULL,
	[TaskTypeId]			int					NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Task] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[TaskType] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskType]
(
--[Id], [Name], [Description], [Image], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[Name]					nvarchar(100)		NOT NULL,
	[Description]			nvarchar(max)		NOT NULL,
	[Image]					nvarchar(100)		NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.TaskType] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[TaskPart] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskPart]
(
--[Id], [TaskId], [PartId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[TaskId]				int					NOT NULL,
	[PartId]				int					NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.TaskPart] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[Part] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Part]
(
--[Id], [Name], [Description], [Image], [PartTypeId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[Name]					nvarchar(100)		NOT NULL,
	[Description]			nvarchar(max)		NOT NULL,
	[Image]					nvarchar(100)		NULL,
	[PartTypeId]			int					NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Part] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[PartType] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartType]
(
--[Id], [Name], [Description], [Image], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[Name]					nvarchar(100)		NOT NULL,
	[Description]			nvarchar(max)		NOT NULL,
	[Image]					nvarchar(100)		NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.PartType] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[Stock] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stock]
(
--[ID], [PartId], [IsAvailable], [StockTypeId], [NumberInStock], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[PartId]				int					NOT NULL,
	[IsAvailable]			bit					NOT NULL,
	[StockTypeId]			int					NOT NULL,
	[NumberInStock]			int					NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Stock] PRIMARY KEY CLUSTERED ( [ID] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[StockType] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockType]
(
--[ID], [IsSingleStockItem], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]
	[Id]					int identity(1,1)	NOT NULL,
	[IsSingleStockItem]		bit					NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.StockType] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[Role] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id]					int identity(1,1)	NOT NULL,
	[Name]					nvarchar(50)		NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[User] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id]					int identity(1,1)	NOT NULL,
	[Username]				nvarchar(100)		NOT NULL,
	[Email]					nvarchar(200)		NOT NULL,
	[HashedPassword]		nvarchar(200)		NOT NULL,
	[Salt]					nvarchar(200)		NOT NULL,
	[IsLocked]				bit					NOT NULL,
	[DateCreated]			datetime			NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED ( [Id] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Table [dbo].[UserRole] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id]					int identity(1,1)	NOT NULL,
	[UserId]				int					NOT NULL,
	[RoleId]				int					NOT NULL,
	[LastChangeUserId]		int					NOT NULL,
	[CreationTimeStamp]		datetime			NOT NULL,
	[LastChangeTimeStamp]	datetime			NOT NULL,
	CONSTRAINT [PK_dbo.UserRole] PRIMARY KEY CLUSTERED ( [ID] ASC )
	WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
) ON [PRIMARY]
GO


INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion])
VALUES (N'201511091712006_initial', N'HoutEnItKlussenBV.Data.Migrations.Configuration', 0x1F8B0800000000000400ED5DCD6EE4B811BE07C83B083A2581D76D7BB0C0C668EFC2D3F6EC36763C1EB83D9BDC0C5AA2DBC2E8A757A21C1BC13E590E79A4BC42288994F82B91925A6D7B8CB9B449F1E34F158B554556CDFFFEF3DFF94F8F51E83CC0340B92F8C43DDC3F701D187B891FC4EB13374777DFFDE0FEF4E39FFF343FF7A347E737FADDBBE23BDC32CE4EDC7B8436C7B359E6DDC30864FB51E0A54996DCA17D2F8966C04F664707077F9F1D1ECE2086703196E3CCAFF21805112CFFC07F2E92D8831B9483F022F1619891725CB32A519D4F2082D90678F0C4FD2589E0228871E9FE1940C0754EC300E061AC6078E73A208E1304101EE4F1970CAE509AC4EBD5061780F0FA6903F1777720CC2019FC71F3B9E93C0E8E8A79CC9A8614CACB339444968087EFC8C2CCC4E6BD96D7AD170E2FDD395E62F454CCBA5CBE137751760153D7113B3B5E8469F121B7BA65FB0066FBB4DD9EA3A8DDAB9902F34EF16FCF59E421CA537812C31CA520DC733EE7B761E0FD0A9FAE93AF303E89F33064478AC78AEBB8025CF4394D3630454F57F08E8C7F79E63A33BEDD4C6C583763DA54335BC6E8DD91EB7CC29D83DB10D68CC0ACC20A2529FC19C6300508FA9F014230C5745CFAB05C4AA977A1AF0F419AA1E227ED12B31FDE46AE73011E3FC2788DEEF1063BC01BE743F0087D5A4286F1250EF0AEC38D509A43C530DBBBFE0876D5F3790482B0A5DBA3ED744B69B200A9DFD2FBF75BE91CB7FE3D8798F568CF3FE7816F8D82A517BCBC7B1FA4E89EE21445D741643FA28BE43608DBA96FB410EDBD5CC17590E12D5D6C9662A896C39ECF1A91D42AA8CED334B1965265A33711D5C12730CBC0BA8D51F0CF1138658580F7F53AC507F6D6BB2A986F91C26239B6C58F78BD5368CB8F65A3377E6C275EC7993592F8FE041E8275395E496C3E607AB8CE150CCBEAEC3ED8549AE47E59754328FF214DA2AB24A42DAAE29B6B90AE21C2C34FE4BA5592A75E5F7E2B816CF9AD6CF4C66FEDFC761DA08E63722B0AC319CCBC34D8547A7D9BAAB41D5D299A42E4977CBFF4BB68D9B150410A3D541CFE5393E81F6980605BBFDB5125F19F7EEEEDA0632CF220C8600F354E8185F76631E80AE6FD13B287C0BA025660D32F57CB116C0953F14FA47B3FE94F25BC4AFAD393C1742058AA795FDBCE21FA813894AA5C331652A91A8CF1515462D81E4565A3B7A3A8CB64C3341A2A2FC7B14497D9E903DE7E458B7A13279891403C54B752B274C91E37A4BEE168A6586268B6CE76735D617AE0A1B70CA5FE421C0CA9D00C87D60EDA601588ED0EAB5ABD6DB176BEA68ECBA1BBAC24F750908A64E39CB710933386BE1AACD332477936DC53D45B2E9013A5FF6614CD2EF556EDB71993D0DAEE2ADABC6DC49D9BF9C634FE92C1B40F9D69BB375A77A824D970815BACB335865E0128A9AD103894A437D5178DC0E12AA4D39FAF1D74FA17507D18F18D09BB9930FE96EEC37E01D97DB14E59F6AFA4F5466C4BFDAF408876700B987DC4677FE3F837B65A46BB4368933B544EA84D8FA2F686F984973E4D8D52FC30D536F2E734CB122F2847C25A69C4D1C14FEB3CF69D16AF4745E9DA4381A98D454DB0C1C205F77BE2FE4D5A26355EADCC3578C4EBC2E31DBAA250BA8CCF600811744EBDEAB9C502641EF065FAE0B5F0F9122CC7605A0812102E304DB0640C62240BBD20F6820D08F5A3169A184ACA624835B858730637302E849C7EED4D7AAD9DB172D7750FC22A752DCA7CC6704F3B53B106BE8E0994D67EC304C44A30672A9587A08B4977CF548A514FC0548AB537E9B5F658ED90A9A875D8CE0692DFA683B1466504DD60145C499D3F5D6C3E80B784A5988CBB84599BF45BBB5976C25F9CA3BDFDE812BDEE538A19CD5814CC652842071C86FC3A4C761AF2537EFE928B373375E4D4D89C0D3D1BC785F9A9A836555909A400DCFDB1A81CF604FCA5A48149BFD461B133FE622C89365E5099153C874D24BC34464C2F76EFCD62F26A4CC463F2CC4D3AA69EB50998AC321F711B845BC0943A196A9F5351031F91C275850749BC571931EA451628905710097734AB82FC8DD54A98A079B22E89381EA67CFDA9C620AF493B004A8B490D400CD20E80F28C510310ADA003A03CD6D400E428EF00A8542E3502D534BB20304F6A00CAADD8D19CB2B51AA2D9D106307A08A939C3C30239E80B0EE60BC5030F713BB57B3EEAC1B22497B664BBB783C168F84E949EFCB40CA6CCDDABCB53D65AE59D763933DC86495BA6ACB2C50D96ADF794EB2B43DDA49556A381DDD86FE2A2C1C7A0305B74F0DCF997413A16571934DD264D3F26176C1183D5EB316DE1D2469E778BBA6DA0703383E6A459CBE4D54A364B753548CFC9B34E63F5EC75CAA0893A28CCDF64EE0A3DCE6C155B16807AB06B15A4AE9BCFAA984052309F698207E71760B309E235134C484A9C551549B8F86E651F65175518338F5B5B5161AA7B42490AD650A8C55DE39196E1624508E32D286E15167E247D262B5C9A8393F627EB5432E9E8914ADB14BF45FDAE0CAD6CB42F593B258D3FE0D945856E5BDED73154D737758AA04E108254713DB848C23C8AF56AB6BE35137BC78230C5E6584D301D0BD5949A2391CB401686149963F0916EDC0A7135E688CCA341168E2936C7E282D85834AEC21C8F06B2B150B4CC1C450E5463F1E45A19793E13385CB2B9A48D2419BFFCDE34DAB9B5193364DB56068FFD9ED5B4DBCE86AD23D1384AD342731C36D28C8562CBEDD8B9BE0816D9B9AE7836EC521B0F43D845690419B08BA6DD76D84596C73A59BC2352D46AF21052288D33035268DA6D871424868A05204516FB8C0D88E2F6195B61714846922C2145E618F59D398BA2BD486F995B1DC3C44DAC2E3547A231492C0E2D334769428C589CA6D4E658656286F81395A9B0C02371431C1429B3E047267488634AA6FCD98889DA101E22262A77A0BD98D0B4DBD2014FEFDC78554E7311A7C719535BE5025DB8A9B115CF86591A6FD1106E21AE5F7B76D135DC0EBFB0C11A2C0A5B6EA31692B704BC4EA87960A0C761A3367889D794DBA0B1811B3C1E5B63A5FE96E11C82EA5B963D1F3E26EE9F415CAC70E699F0B0B2D937AAA3B2AEB821B4A82F72ECE9A16FBA1D9AD03B53EEE8D0DCA3EA51E8F53EB7633557FE3BA5EE1894ED49D529291A4B3BAD299DD63B27BEBB67C1C43A0BA95EBEA6E7647A5962A3E9D097F1BC9A434B5FB84B44BA1E103FA97BAFAF0984EB803971CD77271C947CF5D527AE8317EA21F00B3FFDEA29433022CAF9EFE1220CF07C9B0F2E401CDCC10C553137EED1C1E19190B6F0F9A4109C65991F2AAE36547904797A4D10381414ABDA191A6419FC21A5ED8B1F40EADD83540E171A98956F34602EC848897AD4035595534F09FEBD3DB694A8202F0B82B2CBBBA0F0430C4F9FE7E322343C7D9E864C2CEA906C7986A3B4CC96F73AF6A2909F8E52E22F1178FCABEDFACB39E806C12962C4C6A6A42A0CEBA552B253EE750A11CB7469AF63D9B80465A39D178AFC63BA53A3C7B1118DB563851462E5024B0F4C97B10F1F4FDC7F976D8E9DE53F6F48B33DE732C5CAD6B173E0FC3138F1D8684BCFE7151BEB38FD2CA40D1B0B579115ACF7A1CA6705C3BF9F4A820E4E0C66A2EFC8DC6699F1EA75481321C794E98622CD066DA8F1153E4596AADBA08BA16CF330BD0EBACB898FFA6C3D21ED9129F7906683B8474E96D45F0C299225A9C00C554A267992819D308823E568B197CA8F132A836A07F38B5D393E9B8EE916AC5A0DDB815C0A1ED38EAB56161DDB65CA793D348D5F8E5B489D5A6634783673CC78AE2C21314CB7A23085C1DF2FFD0A89619A34438A10D9D127DF4BAFA84CDD5BC4ED4492BF92542A8624DA66928B69130F4C96C5E2052417B0488BB2B33C2834F874D2CC27533089F669D5CB4D6EF22647BE4D396296A4A48AB99C327F881C6639659608CD2BA7AD251E7955C94676995B646A46D13C9CDA2DA3EC3C61881CE62AD24C9D0CA4351348F5FC061B63B709A6706584D9E509D1270951615BE40FD1270F51015BE415D1271551015BE41BD1271B5101DBE421694942A282B64850A2CD4EA2C4B5C95BD29AB444856E95D2449BCF44872CA16E2FD309C7BE4C789F2C3B247D9169A366BC1D6532311C9E4A09956266C69AD258994ABA87A834C0E4E08EE1131B330DC9B3A1D668494658A9D20424B44D4A9445FCB3F971A636620A116998934CCE223D88FCCE17AB39CC7F3F8E55AC2C583710C57F461E438F5370EA6F96F15D42F52C6144F413E9451B023ED67E4E5314DC010FE16A0F6659F932E23710E6A5DBFE16FACBF832479B1CE129C3E836E4C2DA0A7DADADFF32070A3FE6F965F9DA271B630A789841E1F9BE8CDFE741E8D7E3FEA0F07C6B200A4590DCA014B42C5E45C2F5538DF429890D81C8F2D5FAEB358C362106CB2EE31578807DC686D9EF235C03EF893ED7D6837413825FF6F95900D629883282D1B4C77F621EF6A3C71FFF0F91114602857F0000, N'6.1.3-40302')

SET IDENTITY_INSERT [dbo].[Customer] ON
INSERT [dbo].[Customer] ([Id], [FirstName], [LastName], [Email], [UniqueKey], [DateOfBirth], [Mobile], [Phone], [RegistrationDate], [AddressId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'Klant', N'van der Klant', N'klant@klant.nl', N'40e6b0a3-6ae6-4a9b-8c58-85539037653a', CAST(N'2016-01-01 00:00:00.000' AS DateTime), N'0204567890', N'0612345678', GETDATE(), 1, 1, GETDATE(), GETDATE() )
SET IDENTITY_INSERT [dbo].[Customer] OFF

SET IDENTITY_INSERT [dbo].[Address] ON
INSERT [dbo].[Address] ([Id], [Street], [Number], [NumberExt], [Zipcode], [Town], [Province], [Country], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'Street', N'1', NULL, N'1234AB', N'Stad', N'Provincie', N'Nederland', 1, GETDATE(), GETDATE() )
SET IDENTITY_INSERT [dbo].[Address] OFF

SET IDENTITY_INSERT [dbo].[Project] ON
INSERT [dbo].[Project] ([Id], [Name], [Description], [CustomerId], [DateCreated], [DateReady], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'Restauratie', N'Meubelrestauratie', 1, GETDATE(), GETDATE(), 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[Project] OFF

SET IDENTITY_INSERT [dbo].[ProjectTask] ON
INSERT [dbo].[ProjectTask] ([Id], [ProjectId], [TaskId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, 1, 1, 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[ProjectTask] OFF

SET IDENTITY_INSERT [dbo].[Task] ON
INSERT [dbo].[Task] ([Id], [Name], [Description], [Image], [TaskTypeId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'Stoel', N'Rookstoel', N'stoel.jpg', 1, 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[Task] OFF

SET IDENTITY_INSERT [dbo].[TaskType] ON
INSERT [dbo].[TaskType] ([Id], [Name], [Description], [Image], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'Stoel', N'Stoelreparatie', NULL, 1, GETDATE(), GETDATE() )
SET IDENTITY_INSERT [dbo].[TaskType] OFF

SET IDENTITY_INSERT [dbo].[TaskPart] ON
INSERT [dbo].[TaskPart] ([Id], [TaskId], [PartId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, 1, 1, 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[TaskPart] OFF

SET IDENTITY_INSERT [dbo].[Part] ON
INSERT [dbo].[Part] ([Id], [Name], [Description], [Image], [PartTypeId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'Zitvlak', N'Zitvlak', N'zitvlak.jpg', 1, 1, GETDATE(), GETDATE() )
SET IDENTITY_INSERT [dbo].[Part] OFF

SET IDENTITY_INSERT [dbo].[PartType] ON
INSERT [dbo].[PartType] ([Id], [Name], [Description], [Image], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'Hout', N'Hout', NULL, 1, GETDATE(), GETDATE() )
SET IDENTITY_INSERT [dbo].[PartType] OFF

SET IDENTITY_INSERT [dbo].[Stock] ON
INSERT [dbo].[Stock] ([ID], [PartId], [IsAvailable], [StockTypeId], [NumberInStock], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, 1, 1, 1, 1, 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[Stock] OFF

SET IDENTITY_INSERT [dbo].[StockType] ON
INSERT [dbo].[StockType] ([ID], [IsSingleStockItem], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, 1, 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[StockType] OFF

SET IDENTITY_INSERT [dbo].[Role] ON
INSERT [dbo].[Role] ([ID], [Name], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]) VALUES (1, N'SUPER_ADMIN', 1, GETDATE(), GETDATE())
INSERT [dbo].[Role] ([ID], [Name], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]) VALUES (2, N'BASIC_ADMIN', 1, GETDATE(), GETDATE())
INSERT [dbo].[Role] ([ID], [Name], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]) VALUES (3, N'ELEVATED_USER', 1, GETDATE(), GETDATE())
INSERT [dbo].[Role] ([ID], [Name], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]) VALUES (4, N'BASIC_USER', 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[Role] OFF

SET IDENTITY_INSERT [dbo].[User] ON
INSERT [dbo].[User] ([ID], [Username], [Email], [HashedPassword], [Salt], [IsLocked], [DateCreated], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp])
VALUES (1, N'KWHJ', N'info@van-der-kamp.nl', N'DCSzXo2ckLeB3tKckVZt7K+YtYW1RN6tAu++EMdq7WY=', N'txzhYDuClNHH8B3Lr42NOg==', 0, CAST(N'2016-04-25 00:00:00.001' AS DateTime), 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[User] OFF

SET IDENTITY_INSERT [dbo].[UserRole] ON
INSERT [dbo].[UserRole] ([ID], [UserId], [RoleId], [LastChangeUserId], [CreationTimeStamp], [LastChangeTimeStamp]) VALUES (1, 1, 1, 1, GETDATE(), GETDATE())
SET IDENTITY_INSERT [dbo].[UserRole] OFF


/****** Index [IX_RoleId] ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[UserRole] ( [RoleId] ASC )
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Index [IX_UserId] ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[UserRole] ( [UserId] ASC )
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER TABLE [dbo].[Customer] WITH CHECK ADD CONSTRAINT [FK_Customer_Address] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Address]
GO


ALTER TABLE [dbo].[Project] WITH CHECK ADD CONSTRAINT [FK_Project_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Project] CHECK CONSTRAINT [FK_Project_Customer]
GO


ALTER TABLE [dbo].[ProjectTask]  WITH CHECK ADD  CONSTRAINT [FK_ProjectTask_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Project] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProjectTask] CHECK CONSTRAINT [FK_ProjectTask_Project]
GO


ALTER TABLE [dbo].[ProjectTask]  WITH CHECK ADD  CONSTRAINT [FK_ProjectTask_Task] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Task] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProjectTask] CHECK CONSTRAINT [FK_ProjectTask_Task]
GO


ALTER TABLE [dbo].[Task] WITH CHECK ADD CONSTRAINT [FK_Task_TaskType] FOREIGN KEY([TaskTypeId])
REFERENCES [dbo].[TaskType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_TaskType]
GO


ALTER TABLE [dbo].[TaskPart] WITH CHECK ADD CONSTRAINT [FK_TaskPart_Task] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Task] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TaskPart] CHECK CONSTRAINT [FK_TaskPart_Task]
GO

ALTER TABLE [dbo].[TaskPart] WITH CHECK ADD CONSTRAINT [FK_TaskPart_Part] FOREIGN KEY([PartId])
REFERENCES [dbo].[Part] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TaskPart] CHECK CONSTRAINT [FK_TaskPart_Part]
GO


ALTER TABLE [dbo].[Part] WITH CHECK ADD CONSTRAINT [FK_Part_PartType] FOREIGN KEY([PartTypeId])
REFERENCES [dbo].[PartType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Part] CHECK CONSTRAINT [FK_Part_PartType]
GO


ALTER TABLE [dbo].[Stock] WITH CHECK ADD CONSTRAINT [FK_Stock_Part] FOREIGN KEY([PartId])
REFERENCES [dbo].[Part] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Stock_Part]
GO


ALTER TABLE [dbo].[Stock] WITH CHECK ADD CONSTRAINT [FK_Stock_StockType] FOREIGN KEY([StockTypeId])
REFERENCES [dbo].[StockType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Stock_StockType]
GO


ALTER TABLE [dbo].[UserRole] WITH CHECK ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO


ALTER TABLE [dbo].[UserRole] WITH CHECK ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO


USE [master]
GO
ALTER DATABASE [HoutEnItKlussenBV] SET  READ_WRITE
GO
