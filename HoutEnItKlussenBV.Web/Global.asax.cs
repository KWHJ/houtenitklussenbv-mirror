﻿namespace HoutEnItKlussenBV.Web
{
    using System;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using App_Start;

    public class MvcApplication : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            var config = GlobalConfiguration.Configuration;

            config.Formatters.JsonFormatter
                .SerializerSettings
                .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // Do not change this order to something like alphabetical Don't!!!
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(config);

            Bootstrapper.Run();

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.EnsureInitialized();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

        }
    }
}
