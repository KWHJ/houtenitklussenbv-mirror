﻿namespace HoutEnItKlussenBV.Web.Infrastructure.Validators
{
    using FluentValidation;
    using Models;

    /// <summary>
    /// Class RegistrationViewModelValidator which inherits AbstractValidator of type <RegistrationViewModel>
    /// </summary>
    public class RegistrationViewModelValidator : AbstractValidator<RegistrationViewModel>
    {
        /// <summary>
        /// Initialize RegistrationViewModelValidator
        /// </summary>
        public RegistrationViewModelValidator()
        {
            RuleFor(r => r.Email).NotEmpty().EmailAddress()
                .WithMessage("Invalid email address");

            RuleFor(r => r.Username).NotEmpty()
                .WithMessage("Invalid username");

            RuleFor(r => r.Password).NotEmpty()
                .WithMessage("Invalid password");
        }
    }

    /// <summary>
    /// Class LoginViewModelValidator which inherits AbstractValidator of type <LoginViewModel>
    /// </summary>
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        /// <summary>
        /// Initialize LoginViewModelValidator
        /// </summary>
        public LoginViewModelValidator()
        {
            RuleFor(r => r.Username).NotEmpty()
                .WithMessage("Invalid username");

            RuleFor(r => r.Password).NotEmpty()
                .WithMessage("Invalid password");
        }
    }
}