﻿namespace HoutEnItKlussenBV.Web.Infrastructure.Validators
{
    using FluentValidation;
    using HoutEnItKlussenBV.Web.Models;

    public class StockViewModelValidator : AbstractValidator<StockViewModel>
    {
        public StockViewModelValidator()
        {
            RuleFor(s => s.Id).GreaterThan(0)
                .WithMessage("Invalid stock item");

            //RuleFor(s => s.UniqueKey).NotEqual(Guid.Empty)
            //    .WithMessage("Invalid stock item");
        }
    }
}