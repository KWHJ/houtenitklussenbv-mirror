﻿namespace HoutEnItKlussenBV.Web.Infrastructure.Validators
{
    using FluentValidation;
    using Models;

    public class UserViewModelValidator : AbstractValidator<UserViewModel>
    {
        public UserViewModelValidator()
        {
            //RuleFor(user => user.FirstName).NotEmpty()
            //    .Length(1, 100).WithMessage("First Name must be between 1 - 100 characters");

            //RuleFor(user => user.LastName).NotEmpty()
            //    .Length(1, 100).WithMessage("Last Name must be between 1 - 100 characters");

            //RuleFor(user => user.IdentityCard).NotEmpty()
            //    .Length(1, 100).WithMessage("Identity Card must be between 1 - 50 characters");

            //RuleFor(user => user.DateOfBirth).NotNull()
            //    .LessThan(DateTime.Now.AddYears(-16))
            //    .WithMessage("user must be at least 16 years old.");

            //RuleFor(user => user.Mobile).NotEmpty().Matches(@"^\d{10}$")
            //    .Length(10).WithMessage("Mobile phone must have 10 digits");

            RuleFor(user => user.Email).NotEmpty().EmailAddress()
                .WithMessage("Enter a valid Email address");
        }
    }
}