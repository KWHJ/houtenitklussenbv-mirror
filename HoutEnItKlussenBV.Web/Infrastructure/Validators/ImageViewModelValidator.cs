﻿namespace HoutEnItKlussenBV.Web.Infrastructure.Validators
{
    using FluentValidation;
    using Models;

    public class ImageViewModelValidator : AbstractValidator<ImageViewModel>
    {
        public ImageViewModelValidator()
        {
            RuleFor(image => image.Name).NotEmpty()
                .WithMessage("Select a name");

            RuleFor(image => image.Description).NotEmpty()
                .WithMessage("Select a description");

            //RuleFor(task => task.Image).NotEmpty()
            //    .WithMessage("Select an image");

            //RuleFor(task => task.TaskTypeId).NotEmpty()
            //    .WithMessage("Only Youtube Trailers are supported");

            RuleFor(image => image.LocationUrl).NotEmpty().Must(IsValidLocationUrl)
                .WithMessage("Not a valid path");

        }

        private bool IsValidLocationUrl(string locationUrl)
        {
            var blaLoc = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var blaCbase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;

            var dirname = System.IO.Path.GetDirectoryName(blaLoc);

            return (!string.IsNullOrEmpty(locationUrl) && locationUrl.ToLower().StartsWith(dirname));
        }
    }
}