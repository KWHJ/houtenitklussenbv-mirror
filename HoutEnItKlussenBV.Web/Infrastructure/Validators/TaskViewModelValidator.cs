﻿namespace HoutEnItKlussenBV.Web.Infrastructure.Validators
{
    using FluentValidation;
    using Models;

    public class TaskViewModelValidator : AbstractValidator<TaskViewModel>
    {
        public TaskViewModelValidator()
        {
            RuleFor(task => task.Name).NotEmpty()
                .WithMessage("Select a name");

            RuleFor(task => task.Description).NotEmpty()
                .WithMessage("Select a description");

            //RuleFor(task => task.Image).NotEmpty()
            //    .WithMessage("Select an image");

            //RuleFor(task => task.TaskTypeId).NotEmpty()
            //    .WithMessage("Only Youtube Trailers are supported");

        }
    }
}