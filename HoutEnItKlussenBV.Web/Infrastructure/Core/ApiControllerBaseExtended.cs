﻿namespace HoutEnItKlussenBV.Web.Infrastructure.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using Data.Infrastructure;
    using Data.Repositories;
    using Entities;

    public class ApiControllerBaseExtended : ApiController
    {
        private readonly IDataRepositoryFactory dataRepositoryFactory;
        protected IUnitOfWork unitOfWork;

        protected IEntityBaseRepository<Error> errorsRepository;
        protected IEntityBaseRepository<Task> tasksRepository;
        //protected IEntityBaseRepository<Rental> rentalsRepository;
        protected IEntityBaseRepository<Stock> stocksRepository;
        protected IEntityBaseRepository<Customer> customersRepository;

        protected List<Type> requiredRepositories;

        private HttpRequestMessage RequestMessage;

        public ApiControllerBaseExtended(IDataRepositoryFactory dataRepositoryFactory, IUnitOfWork unitOfWork)
        {
            this.dataRepositoryFactory = dataRepositoryFactory;
            this.unitOfWork = unitOfWork;
        }

        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage request, List<Type> repos, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;

            try
            {
                RequestMessage = request;
                InitRepositories(repos);
                response = function.Invoke();
            }
            catch (DbUpdateException ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }

        private void InitRepositories(List<Type> entities)
        {
            errorsRepository = dataRepositoryFactory.GetDataRepository<Error>(RequestMessage);

            //if (entities.Any(e => e.FullName == typeof(Task).FullName))
            //{
            //    tasksRepository = dataRepositoryFactory.GetDataRepository<Task>(RequestMessage);
            //}

            //if (entities.Any(e => e.FullName == typeof(Rental).FullName))
            //{
            //    rentalsRepository = dataRepositoryFactory.GetDataRepository<Rental>(RequestMessage);
            //}

            if (entities.Any(e => e.FullName == typeof(Customer).FullName))
            {
                customersRepository = dataRepositoryFactory.GetDataRepository<Customer>(RequestMessage);
            }

            if (entities.Any(e => e.FullName == typeof(Stock).FullName))
            {
                stocksRepository = dataRepositoryFactory.GetDataRepository<Stock>(RequestMessage);
            }

            if (entities.Any(e => e.FullName == typeof(User).FullName))
            {
                stocksRepository = dataRepositoryFactory.GetDataRepository<Stock>(RequestMessage);
            }
        }

        private void LogError(Exception ex)
        {
            try
            {
                Error error = new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    DateCreated = DateTime.Now
                };

                errorsRepository.Add(error);
                unitOfWork.Commit();
            }
            catch (Exception e)
            {
                var bla = string.Format("{0} {1}: {2}", GetType().Name, "Logging of errors on implemented error logging process", e.InnerException);
            }
        }
    }
}