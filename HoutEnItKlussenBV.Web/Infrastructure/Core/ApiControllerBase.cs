﻿namespace HoutEnItKlussenBV.Web.Infrastructure.Core
{
    using System;
    using System.Data.Entity.Infrastructure;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using Data.Infrastructure;
    using Data.Repositories;
    using Entities;

    public class ApiControllerBase : ApiController
    {
        protected readonly IEntityBaseRepository<Error> errorsRepository;
        protected readonly IUnitOfWork unitOfWork;

        public ApiControllerBase(IEntityBaseRepository<Error> errorsRepository, IUnitOfWork unitOfWork)
        {
            this.errorsRepository = errorsRepository;
            this.unitOfWork = unitOfWork;
        }

        public ApiControllerBase(IDataRepositoryFactory dataRepositoryFactory, IEntityBaseRepository<Error> errorsRepository, IUnitOfWork unitOfWork)
        {
            this.errorsRepository = errorsRepository;
            this.unitOfWork = unitOfWork;
        }

        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage request, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;

            try
            {
                response = function.Invoke();
            }
            catch (DbUpdateException ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }

        private void LogError(Exception ex)
        {
            try
            {
                Error error = new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    DateCreated = DateTime.Now
                };

                errorsRepository.Add(error);
                unitOfWork.Commit();
            }
            catch (Exception e)
            {
                var bla = string.Format("{0} {1}: {2}", GetType().Name, "Logging of errors on implemented error logging process", e.InnerException);
            }
        }
    }
}