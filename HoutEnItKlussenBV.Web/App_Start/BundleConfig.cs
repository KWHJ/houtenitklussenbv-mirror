﻿namespace HoutEnItKlussenBV.Web
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region "third party scripts"

            #region "modernizr"
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr/modernizr.js"));
            #endregion

            #region "angular"
            bundles.Add(new ScriptBundle("~/bundles/vendors").Include(

                "~/Scripts/vendors/angular.js",
                "~/Scripts/vendors/angular-route.js",
                "~/Scripts/vendors/angular-cookies.js",
                "~/Scripts/vendors/angular-validator.js",
                "~/Scripts/vendors/angular-base64.js",
                "~/Scripts/vendors/angular-file-upload.js",

                "~/Scripts/vendors/angucomplete-alt.min.js",

                "~/Scripts/vendors/jquery-2.2.3.js",

                "~/Scripts/vendors/bootstrap.js",

                "~/Scripts/vendors/loading-bar.js",

                "~/Scripts/vendors/morris.js",

                "~/Scripts/vendors/jquery.raty.js",
                "~/Scripts/vendors/jquery.fancybox.js",
                "~/Scripts/vendors/jquery.fancybox-media.js",

                "~/Scripts/vendors/raphael.js",

                "~/Scripts/vendors/respond.src.js",

                "~/Scripts/vendors/toastr.js",

                "~/Scripts/vendors/ui-bootstrap-tpls-0.13.1.js",

                "~/Scripts/vendors/underscore.js"));
            #endregion

            #endregion "third party scripts"

            #region "single page application"
            bundles.Add(new ScriptBundle("~/bundles/spa").Include(
                "~/Scripts/spa/modules/common.core.js",
                "~/Scripts/spa/modules/common.ui.js",
                "~/Scripts/spa/app.js",
                "~/Scripts/spa/services/apiService.js",
                "~/Scripts/spa/services/notificationService.js",
                "~/Scripts/spa/services/membershipService.js",
                "~/Scripts/spa/services/fileUploadService.js",
                "~/Scripts/spa/layout/topBar.directive.js",
                "~/Scripts/spa/layout/sideBar.directive.js",
                "~/Scripts/spa/layout/customPager.directive.js",
                "~/Scripts/spa/directives/rating.directive.js",
                "~/Scripts/spa/directives/availableTask.directive.js",
                "~/Scripts/spa/about/aboutCtrl.js",
                "~/Scripts/spa/account/loginCtrl.js",
                "~/Scripts/spa/account/registerCtrl.js",
                "~/Scripts/spa/home/rootCtrl.js",
                "~/Scripts/spa/home/indexCtrl.js",
                "~/Scripts/spa/customers/customersCtrl.js",
                "~/Scripts/spa/customers/customersRegCtrl.js",
                "~/Scripts/spa/customers/customerEditCtrl.js",
                "~/Scripts/spa/tasks/tasksCtrl.js",
                "~/Scripts/spa/tasks/taskAddCtrl.js",
                "~/Scripts/spa/tasks/taskDetailsCtrl.js",
                "~/Scripts/spa/tasks/taskEditCtrl.js",
                "~/Scripts/spa/users/usersCtrl.js"));
            #endregion "single page application"

            #region "style sheets"
            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/css/site.css",
                 "~/content/css/bootstrap.css",
                 "~/content/css/bootstrap-theme.css",
                 "~/Content/css/font-awesome.css",
                 "~/Content/css/morris.css",
                 "~/Content/css/toastr.css",
                 "~/Content/css/jquery.fancybox.css",
                 "~/Content/css/loading-bar.css"));
            #endregion "style sheets"

            BundleTable.EnableOptimizations = false;
        }
    }
}
