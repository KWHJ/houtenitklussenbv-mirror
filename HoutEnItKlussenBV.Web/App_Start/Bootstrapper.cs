﻿namespace HoutEnItKlussenBV.Web.App_Start
{
    using System.Web.Http;
    using Mappings;

    public class Bootstrapper
    {
        public static void Run()
        {
            // Configure Autofac
            AutofacWebapiConfig.Initialize(GlobalConfiguration.Configuration);
            //Configure AutoMapper
            AutoMapperConfiguration.Configure();
        }
    }
}