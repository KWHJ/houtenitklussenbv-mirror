﻿(function (app) {
    'use strict';

    app.controller('taskEditCtrl', taskEditCtrl);

    taskEditCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService', 'fileUploadService'];

    function taskEditCtrl($scope, $location, $routeParams, apiService, notificationService, fileUploadService) {


        alert("taskEditCtrl: "
            + "\r\n taskEditCtrl($scope, $location, $routeParams, apiService, notificationService, fileUploadService): "
            + "\r\n $scope: " + $scope
            + "\r\n $location: " + $location
            + "\r\n $routeParams: " + $routeParams
            + "\r\n apiService: " + apiService
            + "\r\n notificationService: " + notificationService
            + "\r\n fileUploadService: " + fileUploadService
            );

        $scope.pageClass = 'page-tasks';
        $scope.task = {};
        $scope.images = [];
        $scope.loadingTask = true;
        $scope.isReadOnly = false;
        $scope.UpdateTask = UpdateTask;
        $scope.prepareFiles = prepareFiles;
        $scope.openDatePicker = openDatePicker;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.datepicker = {};

        var taskImage = null;

        function loadTasks() {

            $scope.loadingTask = true;

            apiService.get('/api/tasks/details/' + $routeParams.id, null,
            tasksLoadCompleted,
            tasksLoadFailed);
        }

        function taskLoadCompleted(result) {

            //alert("taskDetailsCtrl: " +
            //    "\r\n tasksLoadCompleted(result): ");

            $scope.task = result.data;
            $scope.loadingTask = false;

            loadTasks();
        }

        function taskLoadFailed(response) {

            //alert("taskDetailsCtrl: " +
            //    "\r\n tasksLoadFailed(response): ");

            notificationService.displayError(response.data);
        }

        function imagesLoadCompleted(response) {

            $scope.images = response.data;
        }

        function imagesLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function loadImages() {
            apiService.get('/api/images/', null,
            imagesLoadCompleted,
            imagesLoadFailed);
        }

        function UpdateTask() {
            if (taskImage) {
                fileUploadService.uploadImage(taskImage, $scope.task.ID, UpdateTaskModel);
            }
            else {
                UpdateTaskModel();
            }
        }

        function UpdateTaskModel() {
            apiService.post('/api/tasks/update', $scope.task,
            updateTaskSucceeded,
            updateTaskFailed);
        }

        function prepareFiles($files) {
            taskImage = $files;
        }

        function updateTaskSucceeded(response) {
            console.log(response);
            notificationService.displaySuccess($scope.task.Title + ' has been updated');
            $scope.task = response.data;
            taskImage = null;
        }

        function updateTaskFailed(response) {
            notificationService.displayError(response);
        }

        function openDatePicker($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepicker.opened = true;
        };

        loadTask();
    }

})(angular.module('houtEnItKlussenBV'));