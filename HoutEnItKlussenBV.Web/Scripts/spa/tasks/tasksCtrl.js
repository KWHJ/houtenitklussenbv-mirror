﻿(function (app) {
    'use strict';

    app.controller('tasksCtrl', tasksCtrl);

    tasksCtrl.$inject = ['$scope', 'apiService','notificationService'];

    function tasksCtrl($scope, apiService, notificationService) {
        $scope.pageClass = 'page-tasks';
        $scope.loadingTasks = true;
        $scope.page = 0;
        $scope.pagesCount = 0;

        $scope.Tasks = [];

        $scope.search = search;
        $scope.clearSearch = clearSearch;

        function search(page) {
            page = page || 0;

            $scope.loadingTasks = true;

            var config = {
                params: {
                    page: page,
                    pageSize: 6,
                    filter: $scope.filterTasks
                }
            };

            apiService.get('/api/tasks/', config,
            tasksLoadCompleted,
            tasksLoadFailed);
        }

        function tasksLoadCompleted(result) {

            $scope.Tasks = result.data.Items;
            $scope.page = result.data.Page;
            $scope.pagesCount = result.data.TotalPages;
            $scope.totalCount = result.data.TotalCount;
            $scope.loadingTasks = false;

            if ($scope.filterTasks && $scope.filterTasks.length)
            {
                notificationService.displayInfo(result.data.Items.length + ' tasks found');
            }
        }

        function tasksLoadFailed(response) {

            //alert("tasksCtrl: " +
            //    "\r\n tasksLoadFailed(response): " +
            //    "\r\n response" + response);

            notificationService.displayError(response.data);
        }

        function clearSearch() {

            //alert("tasksCtrl: " +
            //    "\r\n clearSearch: " );

            $scope.filterTasks = '';
            search();
        }

        $scope.search();
    }

})(angular.module('houtEnItKlussenBV'));