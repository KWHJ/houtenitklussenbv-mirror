﻿(function (app) {
    'use strict';

    app.controller('taskAddCtrl', taskAddCtrl);

    taskAddCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService', 'fileUploadService'];

    function taskAddCtrl($scope, $location, $routeParams, apiService, notificationService, fileUploadService) {


        alert("taskAddCtrl: "
            + "\r\n taskAddCtrl($scope, $location, $routeParams, apiService, notificationService, fileUploadService): "
            + "\r\n $scope: " + $scope
            + "\r\n $location: " + $location
            + "\r\n $routeParams: " + $routeParams
            + "\r\n notificationService: " + notificationService
            + "\r\n fileUploadService: " + fileUploadService
            );

        $scope.pageClass = 'page-tasks';
        //$scope.task = { ImageId: 1 };

        $scope.images = [];
        $scope.isReadOnly = false;

        $scope.AddTask = addTask;
        $scope.prepareFiles = prepareFiles;
        $scope.openDatePicker = openDatePicker;
        $scope.changeNumberOfStocks = changeNumberOfStocks;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.datepicker = {};

        var taskImage = null;

        function loadImages() {
            apiService.get('/api/images/', null,
            imagesLoadCompleted,
            imagesLoadFailed);
        }

        function imagesLoadCompleted(response) {
            $scope.images = response.data;
        }

        function imagesLoadFailed(response) {

            notificationService.displayError(response.data);
        }

        function addTask() {

            addTaskModel();
        }

        function addTaskModel() {

            apiService.post('/api/images/add', $scope.image,
            addTaskSucceeded,
            addTaskFailed);
        }

        function prepareFiles($files) {

            //alert("tasksCtrl: " +
            //    "\r\n prepareFiles($files): ");

            taskImage = $files;
        }

        function addTaskSucceeded(response) {

            //alert("tasksCtrl: " +
            //    "\r\n addTaskSucceded(response): ");

            notificationService.displaySuccess($scope.task.Title + ' has been submitted to Hout & IT KLussen BV');
            $scope.task = response.data;

            if (taskImage) {
                fileUploadService.uploadImage(taskImage, $scope.task.ID, redirectToEdit);
            }
            else
                redirectToEdit();
        }

        function addTaskFailed(response) {

            //alert("tasksCtrl: " +
            //    "\r\n addTaskFailed(response): ");

            console.log(response);
            notificationService.displayError(response.statusText);
        }

        function openDatePicker($event) {

            //alert("tasksCtrl: " +
            //    "\r\n openDatePicker($event): ");

            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepicker.opened = true;
        };

        function redirectToEdit() {

            //alert("tasksCtrl: " +
            //    "\r\n redirectToEdit(): ");

            $location.url('tasks/edit/' + $scope.task.ID);
        }

        function changeNumberOfStocks($vent)
        {

            //alert("tasksCtrl: " +
            //    "\r\n changeNumberOfStocks($vent): ");

            var btn = $('#btnSetStocks'),
            oldValue = $('#inputStocks').val().trim(),
            newVal = 0;

            if (btn.attr('data-dir') == 'up') {
                newVal = parseInt(oldValue) + 1;
            } else {
                if (oldValue > 1) {
                    newVal = parseInt(oldValue) - 1;
                } else {
                    newVal = 1;
                }
            }
            $('#inputStocks').val(newVal);
            $scope.task.NumberOfStocks = newVal;
            console.log($scope.task);
        }

        loadImages();
    }

})(angular.module('houtEnItKlussenBV'));