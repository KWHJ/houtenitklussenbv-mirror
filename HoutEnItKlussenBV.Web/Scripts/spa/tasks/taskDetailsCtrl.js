﻿(function (app) {
    'use strict';

    app.controller('taskDetailsCtrl', taskDetailsCtrl);

    taskDetailsCtrl.$inject = ['$scope', '$location', '$routeParams', '$modal', 'apiService', 'notificationService'];

    function taskDetailsCtrl($scope, $location, $routeParams, $modal, apiService, notificationService) {

        alert("taskDetailsCtrl: "
            + "\r\n taskDetailsCtrl($scope, $location, $routeParams, $modal, apiService, notificationService): "
            + "\r\n $scope: " + $scope
            + "\r\n $location: " + $location
            + "\r\n $routeParams: " + $routeParams
            + "\r\n $modal: " + $modal
            + "\r\n apiService: " + apiService
            + "\r\n notificationService: " + notificationService
            );


        $scope.pageClass = 'page-tasks';
        $scope.task = {};
        $scope.loadingTask = true;
        $scope.loadingRentals = true;
        $scope.isReadOnly = true;

        $scope.getStatusColor = getStatusColor;
        $scope.clearSearch = clearSearch;

        function loadTask() {

            $scope.loadingTask = true;

            apiService.get('/api/tasks/details/' + $routeParams.id, null,
            taskLoadCompleted,
            taskLoadFailed);
        }

        function loadTaskDetails() {
            loadTask();
        }

        function getStatusColor(status) {
            if (status == 'Borrowed')
                return 'red'
            else {
                return 'green';
            }
        }

        function clearSearch() {
            $scope.filterRentals = '';
        }

        function taskLoadCompleted(result) {
            $scope.task = result.data;
            $scope.loadingTask = false;
        }

        function taskLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function rentalHistoryLoadCompleted(result) {
            console.log(result);
            $scope.rentalHistory = result.data;
            $scope.loadingRentals = false;
        }

        function rentalHistoryLoadFailed(response) {
            notificationService.displayError(response);
        }

        function returnTaskSucceeded(response) {
            notificationService.displaySuccess('Task returned to Hout & It Klussen BV successfully');
            loadTaskDetails();
        }

        function returnTaskFailed(response) {
            notificationService.displayError(response.data);
        }

        loadTaskDetails();
    }

})(angular.module('houtEnItKlussenBV'));