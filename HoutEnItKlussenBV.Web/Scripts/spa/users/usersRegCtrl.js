﻿(function (app) {
    'use strict';

    app.controller('usersRegCtrl', usersRegCtrl);

    usersRegCtrl.$inject = ['$scope', '$location', '$rootScope', 'apiService'];

    function usersRegCtrl($scope, $location, $rootScope, apiService) {

        $scope.newUser = {};

        $scope.Register = Register;

        $scope.openDatePicker = openDatePicker;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.datepicker = {};

        $scope.submission = {
            successMessages: ['Successfull submission will appear here.'],
            errorMessages: ['Submition errors will appear here.']
        };

        function Register() {
            apiService.post('/api/users/register', $scope.newUser,
           registerUserSucceded,
           registerUserFailed);
        }

        function registerUserSucceded(response) {
            $scope.submission.errorMessages = ['Submition errors will appear here.'];
            console.log(response);
            var userRegistered = response.data;
            $scope.submission.successMessages = [];
            $scope.submission.successMessages.push($scope.newUser.LastName + ' has been successfully registed');
            $scope.submission.successMessages.push('Check ' + userRegistered.UniqueKey + ' for reference number');
            $scope.newUser = {};
        }

        function registerUserFailed(response) {
            console.log(response);
            if (response.status == '400')
                $scope.submission.errorMessages = response.data;
            else
                $scope.submission.errorMessages = response.statusText;
        }

        function openDatePicker($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepicker.opened = true;
        };
    }

})(angular.module('houtEnItKlussenBV'));