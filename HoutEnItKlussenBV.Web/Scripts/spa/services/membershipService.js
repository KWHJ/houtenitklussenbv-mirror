﻿(function (app) {
    'use strict';

    app.factory('membershipService', membershipService);

    membershipService.$inject = ['apiService', 'notificationService', '$http', '$base64', '$cookieStore', '$rootScope'];
    function membershipService(apiService, notificationService, $http, $base64, $cookieStore, $rootScope) {

        var service = {
            login: login,
            register: register,
            saveCredentials: saveCredentials,
            removeCredentials: removeCredentials,
            isUserLoggedIn: isUserLoggedIn,
            currentUserRoleName: currentUserRoleName,
            currentUserRole: currentUserRole
        }

        function login(user, completed) {
            //alert("Calling api/account/authenticate");
            apiService.post('/api/account/authenticate', user, completed, loginFailed);
        }

        function register(user, completed) {
            apiService.post('/api/account/register', user,
            completed,
            registrationFailed);
        }

        function saveCredentials(user, userrolename, userrole) {
            var membershipData = $base64.encode(user.username + ':' + user.password);

            $rootScope.repository = {
                loggedUser: {
                    username: user.username,
                    authdata: membershipData,
                    userrolename: userrolename,
                    userrole: userrole
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Basic ' + membershipData;
            $cookieStore.put('repository', $rootScope.repository);
        }

        function removeCredentials() {
            $rootScope.repository = {};
            $cookieStore.remove('repository');
            $http.defaults.headers.common.Authorization = '';
        };

        function isUserLoggedIn() {
            //if (typeof $rootScope.repository == 'undefined') {
            //    return false;
            //}
            //else {
                return $rootScope.repository.loggedUser != null;
            //}
        }

        // database Name
        // Do not change order of these enums
        var userRolesNames = {'basic_user': 4, 'elevated_user': 3, 'basic_admin': 2, 'super_admin': 1}
        function currentUserRoleName(roleId) {
            var bla = roleId;
            return find_key_by_value(userRolesNames, roleId);
        }

        // human readable names
        var userRoles = {'Standard User': 4, 'Elevated User': 3, 'Standard Admin': 2, 'Principal Admin': 1}
        function currentUserRole(roleId) {
            var bla = roleId;
            return find_key_by_value(userRoles, roleId);
        }

        //#region Private membershipService.js functions
        function loginFailed(response) {
            notificationService.displayError(response.data);
        }

        function registrationFailed(response) {
            notificationService.displayError('Registration failed. Try again.');
        }
        //#endregion Private functions

        //#region Global functions TODO MOVE TO SEPARATE FILE
        //function convert_enum_to_array(enum) {
        //    var all = [];
        //    for (var key in enum) {
        //        all.push(enum[key]);
        //    }
        //    enum.all = all;
        //}

        function find_key_by_value(array, value) {
            for (var item in array) {
                if (array.hasOwnProperty(item)) {
                    if (array[item] == value) {
                        return item;
                    }
                }
            }
            return undefined;
        }

        function find_value_by_key(array, value) {
            for (var item in array) {
                if (array.hasOwnProperty(item)) {
                    if (array[item] == value) {
                        return item;
                    }
                }
            }
            return undefined;
        }
        //#endregion Global functions

        return service;
    }
})(angular.module('common.core'));