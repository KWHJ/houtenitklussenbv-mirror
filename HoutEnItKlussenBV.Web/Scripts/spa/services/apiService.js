﻿(function (app) {
    'use strict';

    app.factory('apiService', apiService);

    apiService.$inject = ['$http', '$location', 'notificationService', '$rootScope'];

    function apiService($http, $location, notificationService, $rootScope) {
        var service = {
            get: get,
            post: post
        };

        function get(url, config, success, failure) {

             //alert("apiService.js:" +
             //               "\r\n get(.....)" +
             //               "\r\n url: " + url +
             //               "\r\n config: " + config +
             //               "\r\n success: " + success +
             //               "\r\n failure: " + failure );

            return $http.get(url, config)
                    .then(function (result) {

                        //alert("apiService.js:" +
                        //    "\r\n --get--" +
                        //    "\r\n The WRAPPER methode override" +
                        //    "\r\n Task<HttpResponseMessage> SendAsync( )" +
                        //    "\r\n >>> HttpResponseMessage Get(HttpRequestMessage request)" +
                        //    "\r\n HAS BEEN CALLED!!!!" );

                        success(result);
                    }, function (error) {
                        if (error.status == '401') {
                            //redirect back to login page
                            notificationService.displayError('Authentication required.');
                            $rootScope.previousState = $location.path();
                            $location.path('/login');
                        }
                        else if (failure != null) {
                            failure(error);
                        }
                    });
        }

        function post(url, data, success, failure) {

            //alert("apiService.js:" +
            //               "\r\n post(.....)" +
            //               "\r\n url: " + url +
            //               "\r\n config: " + config +
            //               "\r\n success: " + success +
            //               "\r\n failure: " + failure );

            return $http.post(url, data)
                .then(function (result) {

                    //alert("apiService.js:" +
                    //    "\r\n --POST--" +
                    //    "\r\n The WRAPPER methode override" +
                    //    "\r\n Task<HttpResponseMessage> SendAsync( )" +
                    //    "\r\n >>> HttpResponseMessage Get(HttpRequestMessage request)" +
                    //    "\r\n HAS BEEN CALLED!!!!" );

                    success(result);
                }, function (error) {
                    if (error.status == '401') {
                        notificationService.displayError('Authentication required.');
                        $rootScope.previousState = $location.path();
                        $location.path('/login');
                    }
                    else if (failure != null) {
                        failure(error);
                    }
                });
        }

        return service;
    }

})(angular.module('common.core'));