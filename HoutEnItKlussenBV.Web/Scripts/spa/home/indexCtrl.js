﻿(function (app) {
    'use strict';

    //alert("hier2");

    app.controller('indexCtrl', indexCtrl);

    indexCtrl.$inject = ['$scope', 'apiService', 'notificationService'];
    function indexCtrl($scope, apiService, notificationService) {

        $scope.pageClass = 'page-home';
        $scope.loadingTasks = true;
        $scope.isReadOnly = true;

        $scope.latestTasks = [];
        $scope.loadData = loadData;

        function loadData() {

            apiService.get('/api/tasks/latest', null,
                tasksLoadCompleted,
                tasksLoadFailed);
        }

        function tasksLoadCompleted(result) {
            //alert("tasksLoadCompleted: " + result.data);

            var json = JSON.stringify(result.data)
            $scope.latestTasks = result.data;
            $scope.loadingTasks = false;
        }

        function tasksLoadFailed(response) {
            //alert("tasksLoadFailed: " + response.data);
            notificationService.displayError(response.data);
        }

        loadData();

    }

})(angular.module('houtEnItKlussenBV'));