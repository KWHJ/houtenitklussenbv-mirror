﻿(function (app) {
    'use strict';

    //alert("hier1");

    app.controller('rootCtrl', rootCtrl);

    rootCtrl.$inject = ['$scope','$location', 'membershipService','$rootScope'];
    function rootCtrl($scope, $location, membershipService, $rootScope) {

        $scope.userData = {};
        $scope.userData.displayUserInfo = displayUserInfo;
        $scope.logout = logout;

        function displayUserInfo() {
            $scope.userData.isUserLoggedIn = membershipService.isUserLoggedIn();

            if($scope.userData.isUserLoggedIn)
            {
                $scope.username = $rootScope.repository.loggedUser.username;
                $scope.userrolename = $rootScope.repository.loggedUser.userrolename;
                $scope.userrole = $rootScope.repository.loggedUser.userrole;
            }
        }

        function logout() {
            membershipService.removeCredentials();
            $location.path('#/');
            $scope.userData.displayUserInfo();
        }

        $scope.userData.displayUserInfo();

    }

})(angular.module('houtEnItKlussenBV'));