﻿(function (app) {
    'use strict';

    app.controller('loginCtrl', loginCtrl);

    loginCtrl.$inject = ['$scope', 'membershipService', 'notificationService', '$rootScope', '$location'];
    function loginCtrl($scope, membershipService, notificationService, $rootScope, $location) {

        $scope.pageClass = 'page-login';
        $scope.login = login;
        $scope.user = {};

        function login() {
            membershipService.login($scope.user, loginCompleted)
        }

        function loginCompleted(result) {

            if (result.data.success) {

                var userrolename = membershipService.currentUserRoleName(result.data.roleId);
                var userrole = membershipService.currentUserRole(result.data.roleId);

                membershipService.saveCredentials($scope.user, userrolename, userrole);

                notificationService.displayInfo('Your rolename: ' + userrolename);
                notificationService.displayInfo('Your role: ' + userrole);
                notificationService.displaySuccess('Hello ' + $scope.user.username);

                $scope.userData.displayUserInfo();

                if ($rootScope.previousState) {
                    $location.path($rootScope.previousState);
                }
                else {
                    $location.path('/');
                }
            }
            else {
                notificationService.displayError('Login failed. Try again.');
            }
        }
    }

})(angular.module('common.core'));