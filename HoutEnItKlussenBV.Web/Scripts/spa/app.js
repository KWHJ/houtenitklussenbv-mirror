﻿(function () {
    'use strict';

    angular.module('houtEnItKlussenBV', ['common.core', 'common.ui'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "scripts/spa/home/index.html",
                controller: "indexCtrl"
            })

            .when("/about", {
                templateUrl: "scripts/spa/about/about.html",
                controller: "aboutCtrl"
            })

            .when("/customers", {
                templateUrl: "scripts/spa/customers/customers.html",
                controller: "customersCtrl"
            })
            .when("/customers/register", {
                templateUrl: "scripts/spa/customers/register.html",
                controller: "customersRegCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })

            .when("/login", {
                templateUrl: "scripts/spa/account/login.html",
                controller: "loginCtrl"
            })

            .when("/tasks", {
                templateUrl: "scripts/spa/tasks/tasks.html",
                controller: "tasksCtrl"
            })
            .when("/tasks/add", {
                templateUrl: "scripts/spa/tasks/add.html",
                controller: "taskAddCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/tasks/:id", {
                templateUrl: "scripts/spa/tasks/details.html",
                controller: "taskDetailsCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/tasks/edit/:id", {
                templateUrl: "scripts/spa/tasks/edit.html",
                controller: "taskEditCtrl"
            })

            //.when("/rental", {
            //    templateUrl: "scripts/spa/rental/rental.html",
            //    controller: "rentStatsCtrl"
            //})

            .when("/users", {
                templateUrl: "scripts/spa/users/users.html",
                controller: "usersCtrl"
            })
            .when("/users/register", {
                templateUrl: "scripts/spa/users/register.html",
                controller: "usersRegCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })

            .when("/register", {
                templateUrl: "scripts/spa/account/register.html",
                controller: "registerCtrl"
            })

            .otherwise({ redirectTo: "/" });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http', 'membershipService'];

    function run($rootScope, $location, $cookieStore, $http, membershipService) {

        var routesThatDontRequireAuth = ['/login'];
        var routesThatForAdmins = ['/admin'];

        // check if current location matches route
        var routeClean = function (route) {
            return _.find(routesThatDontRequireAuth,
                function (noAuthRoute) {
                    return noAuthRoute.startsWith(route, noAuthRoute);
                });
        };

        var routeAdmin = function (route) {
            return _.find(routesThatForAdmins,
                function (yesAuthRoute) {
                    return yesAuthRoute.startsWith(route, yesAuthRoute);
                });
        };

        $rootScope.$on('$routeChangeStart', function (ev, to, toParams, from, fromParamsnt) {

        });

        // handle page refreshes
        $rootScope.repository = $cookieStore.get('repository') || {};
        if ($rootScope.repository.loggedUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.repository.loggedUser.authdata;
        }

        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });

            $('.fancybox-media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });

            $('[data-toggle=offcanvas]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });
        });
    }

    isAuthenticated.$inject = ['membershipService', '$rootScope', '$location'];
    function isAuthenticated(membershipService, $rootScope, $location) {
        if (!membershipService.isUserLoggedIn()) {
            $rootScope.previousState = $location.path();
            $location.path('/login');
        }
    }
})();

