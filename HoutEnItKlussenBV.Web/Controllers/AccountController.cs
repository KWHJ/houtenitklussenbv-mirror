﻿namespace HoutEnItKlussenBV.Web.Controllers
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using Data.Infrastructure;
     using Data.Repositories;
    using Entities;
    using Infrastructure.Core;
    using Models;
    using Services;
    using Services.Utilities;

    [Authorize(Roles = "SUPER_ADMIN")]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiControllerBase
    {
        private readonly IMembershipService membershipService;

        public AccountController(
            IMembershipService membershipService,
            IEntityBaseRepository<Error> errorsRepository,
            IUnitOfWork unitOfWork) : base(errorsRepository, unitOfWork)
        {
            this.membershipService = membershipService;
        }

        /// <summary>
        /// HttpResponseMessage Login
        /// </summary>
        /// <param name="request">HttpRequestMessage request</param>
        /// <param name="user">LoginViewModel user</param>
        /// <returns>the HttpResponseMessage of Login</returns>
        [AllowAnonymous]
        [Route("authenticate")]
        [HttpPost]
        public HttpResponseMessage Login(HttpRequestMessage request, LoginViewModel user)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;

                    if (ModelState.IsValid)
                    {
                        MembershipContext userContext = membershipService.ValidateUser(user.Username, user.Password);

                        if (userContext.User != null)
                        {
                            var roleId = "";
                            foreach (var item in userContext.User.UserRoles) {
                                if (item.UserId == userContext.User.Id)
                                {
                                    roleId = Convert.ToString(item.RoleId);
                                }
                            }
                            response = request.CreateResponse(HttpStatusCode.OK, new { success = true, roleId = roleId });
                        }
                        else
                        {
                            response = request.CreateResponse(HttpStatusCode.OK, new { success = false });
                        }
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, new { success = false });
                    }

                    return response;
                });
        }

        /// <summary>
        /// Methode HttpResponseMessage Register
        /// </summary>
        /// <remark>
        /// Initial registration with basic rights only: Table Role ID = 3 as integer
        /// </remark>
        /// <param name="request">HttpRequestMessage request</param>
        /// <param name="user">RegistrationViewModel user</param>
        /// <returns>the HttpResponseMessage of Registration</returns>
        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public HttpResponseMessage Register(HttpRequestMessage request, RegistrationViewModel user)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;

                    if (!ModelState.IsValid)
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, new { success = false });
                    }
                    else
                    {
                        User _user = membershipService.CreateUser(user.Username, user.Email, user.Password, new int[] { 3 });

                        if (_user != null)
                        {
                            response = request.CreateResponse(HttpStatusCode.OK, new { success = true });
                        }
                        else
                        {
                            response = request.CreateResponse(HttpStatusCode.OK, new { success = false });
                        }
                    }

                    return response;
                });
        }
    }
}
