﻿namespace HoutEnItKlussenBV.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using System.Web.Http;
    using AutoMapper;
    using Infrastructure.Core;
    using Data.Infrastructure;
    using Entities;
    using Models;

    [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER, BASIS_USER")]
    [RoutePrefix("api/tasksextended")]
    public class TasksExtendedController : ApiControllerBaseExtended
    {
        public TasksExtendedController(
            IDataRepositoryFactory dataRepositoryFactory,
            IUnitOfWork unitOfWork) : base(dataRepositoryFactory, unitOfWork)
        { }

        [AllowAnonymous]
        [Route("latest")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            requiredRepositories = new List<Type>() { typeof(Task) };

            return CreateHttpResponse(request, requiredRepositories, () =>
            {
                var tasks = tasksRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(6).ToList();

                HttpResponseMessage response = null;

                //////////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM
                //////var config = new MapperConfiguration(cfg =>
                //////{
                //////    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //////    cfg.CreateMap<Task, TaskViewModel>();
                //////});
                //////IEnumerable<TaskViewModel> tasksVM = config.CreateMapper().Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
                //////////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM

                IEnumerable<TaskViewModel> tasksVM = Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);

                response = request.CreateResponse(HttpStatusCode.OK, tasksVM);

                return response;

            });
        }

        [Route("details/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            requiredRepositories = new List<Type>() { typeof(Task) };

            return CreateHttpResponse(request, requiredRepositories, () =>
            {
                var task = tasksRepository.GetSingle(id);

                HttpResponseMessage response = null;

                //////////////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM
                //////var config = new MapperConfiguration(cfg =>
                //////{
                //////    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //////    cfg.CreateMap<Task, TaskViewModel>();
                //////});
                //////TaskViewModel taskVM = config.CreateMapper().Map<Task, TaskViewModel>(task);
                //////////////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM

                TaskViewModel taskVM = Mapper.Map<Task, TaskViewModel>(task);

                response = request.CreateResponse(HttpStatusCode.OK, taskVM);
                return response;
            });
        }

        [AllowAnonymous]
        [Route("{page:int=0}/{pageSize=3}/{filter?}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            requiredRepositories = new List<Type>() { typeof(Task) };
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, requiredRepositories, () =>
            {
                List<Task> tasks = null;
                int totalTasks = new int();

                if (!string.IsNullOrEmpty(filter))
                {
                    tasks = tasksRepository.GetAll()
                        .OrderBy(m => m.Id)
                        .Where(m => m.Name.ToLower()
                        .Contains(filter.ToLower().Trim()))
                        .ToList();
                }
                else
                {
                    tasks = tasksRepository.GetAll().ToList();
                }

                totalTasks = tasks.Count();
                tasks = tasks.Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .ToList();

                HttpResponseMessage response = null;

                //////////////////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM
                //////var config = new MapperConfiguration(cfg =>
                //////{
                //////    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //////    cfg.CreateMap<Task, TaskViewModel>();
                //////});
                //////IEnumerable<TaskViewModel> tasksVM = config.CreateMapper().Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
                //////////////////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM

                IEnumerable<TaskViewModel> tasksVM = Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);

                PaginationSet<TaskViewModel> pagedSet = new PaginationSet<TaskViewModel>()
                {
                    Page = currentPage,
                    TotalCount = totalTasks,
                    TotalPages = (int)Math.Ceiling((decimal)totalTasks / currentPageSize),
                    Items = tasksVM
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;

            });
        }

        [HttpPost]
        [Route("add")]
        public HttpResponseMessage Add(HttpRequestMessage request, TaskViewModel task)
        {
            requiredRepositories = new List<Type>() { typeof(Task), typeof(Stock) };

            return CreateHttpResponse(request, requiredRepositories, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                //else
                //{
                //    Task newTask = new Task();
                //    newTask.UpdateTask(task);

                //    for (int i = 0; i < task.NumberOfStocks; i++)
                //    {
                //        Stock stock = new Stock()
                //        {
                //            IsAvailable = true,
                //            Task = newTask,
                //            UniqueKey = Guid.NewGuid()
                //        };
                //        newTask.Stocks.Add(stock);
                //    }

                //    tasksRepository.Add(newTask);

                //    unitOfWork.Commit();

                //    // Update view model
                //    //var config = new MapperConfiguration(cfg =>
                //    //{
                //    //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //    //    cfg.CreateMap<Task, TaskViewModel>();
                //    //});
                //    //task = config.CreateMapper().Map<Task, TaskViewModel>(newTask);

                //    task = Mapper.Map<Task, TaskViewModel>(newTask);

                //    response = request.CreateResponse<TaskViewModel>(HttpStatusCode.Created, task);
                //}

                return response;

            });
        }

        [HttpPost]
        [Route("update")]
        public HttpResponseMessage Update(HttpRequestMessage request, TaskViewModel task)
        {
            requiredRepositories = new List<Type>() { typeof(Task) };

            return CreateHttpResponse(request, requiredRepositories, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var taskDb = tasksRepository.GetSingle(task.Id);
                    if (taskDb == null)
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid task.");
                    }
                    //else
                    //{
                    //    taskDb.UpdateTask(task);
                    //    task.Image = taskDb.Image;
                    //    tasksRepository.Edit(taskDb);

                    //    unitOfWork.Commit();
                    //    response = request.CreateResponse<TaskViewModel>(HttpStatusCode.OK, task);
                    //}
                }

                return response;
            });
        }

        [MimeMultipart]
        [Route("images/upload")]
        public HttpResponseMessage Post(HttpRequestMessage request, int taskId)
        {
            requiredRepositories = new List<Type>() { typeof(Task) };

            return CreateHttpResponse(request, requiredRepositories, () =>
            {
                HttpResponseMessage response = null;

                var taskOld = tasksRepository.GetSingle(taskId);
                if (taskOld == null)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid task.");
                }
                else
                {
                    var uploadPath = HttpContext.Current.Server.MapPath("~/Content/images/tasks");
                    var multipartFormDataStreamProvider = new UploadMultipartFormProvider(uploadPath);

                    // Read the MIME multipart asynchronously
                    Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                    string _localFileName = multipartFormDataStreamProvider
                        .FileData.Select(multiPartData => multiPartData.LocalFileName).FirstOrDefault();

                    // Create response
                    FileUploadResult fileUploadResult = new FileUploadResult
                    {
                        LocalFilePath = _localFileName,
                        FileName = Path.GetFileName(_localFileName),
                        FileLength = new FileInfo(_localFileName).Length
                    };

                    // update database
                    taskOld.LastChangeTimeStamp = DateTime.Now;
                    tasksRepository.Edit(taskOld);
                    unitOfWork.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK, fileUploadResult);
                }

                return response;
            });
        }
    }
}
