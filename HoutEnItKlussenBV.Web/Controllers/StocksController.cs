﻿using HomeCinema.Data.Infrastructure;
using HomeCinema.Data.Repositories;
using HomeCinema.Entities;
using HomeCinema.Web.Infrastructure.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HomeCinema.Data.Extensions;
using HomeCinema.Web.Models;
using AutoMapper;
using HomeCinema.Web.Mappings;

namespace HomeCinema.Web.Controllers
{
    [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER, BASIS_USER")]
    [RoutePrefix("api/stocks")]
    public class StocksController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<Stock> _stocksRepository;
        public StocksController(IEntityBaseRepository<Stock> stocksRepository,
            IEntityBaseRepository<Error> _errorsRepository, IUnitOfWork _unitOfWork)
            : base(_errorsRepository, _unitOfWork)
        {
            _stocksRepository = stocksRepository;
        }

        [Route("movie/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            IEnumerable<Stock> stocks = null;

            return CreateHttpResponse(request, () =>
            {
                stocks = _stocksRepository.GetAvailableItems(id);

                HttpResponseMessage response = null;

                //var config = new MapperConfiguration(cfg =>
                //{
                //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //    cfg.CreateMap<Stock, StockViewModel>();
                //});
                //IEnumerable<StockViewModel> stocksVM = config.CreateMapper().Map<IEnumerable<Stock>, IEnumerable<StockViewModel>>(stocks);

                //--
                IEnumerable<StockViewModel> stocksVM = Mapper.Map<IEnumerable<Stock>, IEnumerable<StockViewModel>>(stocks);
                //--

                response = request.CreateResponse(HttpStatusCode.OK, stocksVM);
                return response;

            });
        }
    }
}
