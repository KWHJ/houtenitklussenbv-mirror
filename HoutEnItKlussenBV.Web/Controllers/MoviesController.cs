﻿using AutoMapper;
using HomeCinema.Data.Infrastructure;
using HomeCinema.Data.Repositories;
using HomeCinema.Entities;
using HomeCinema.Web.Infrastructure.Core;
using HomeCinema.Web.Infrastructure.Extensions;
using HomeCinema.Web.Mappings;
using HomeCinema.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
namespace HomeCinema.Web.Controllers
{
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using System.Web.Http;

    [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER, BASIS_USER")]
    [RoutePrefix("api/movies")]
    public class MoviesController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<Movie> moviesRepository;

        public MoviesController(
            IEntityBaseRepository<Movie> moviesRepository,
            IEntityBaseRepository<Error> errorsRepository,
            IUnitOfWork unitOfWork) : base(errorsRepository, unitOfWork)
        {
            this.moviesRepository = moviesRepository;
        }

        [AllowAnonymous]
        [Route("latest")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var movies = moviesRepository.GetAll().OrderByDescending(m => m.ReleaseDate).Take(6).ToList();

                //--TEST
                //HttpResponseMessage response_TEST = null;
                //var config = new MapperConfiguration(cfg =>
                //{
                //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //    cfg.CreateMap<Movie, MovieViewModel>();
                //});
                //IEnumerable<MovieViewModel> moviesVM_TEST = config.CreateMapper().Map<IEnumerable<Movie>, IEnumerable<MovieViewModel>>(movies);
                //response_TEST = request.CreateResponse(HttpStatusCode.OK, moviesVM_TEST);
                //var response_TEST_Content = response_TEST.Content;
                //--TEST

                IEnumerable<MovieViewModel> moviesVM = Mapper.Map<IEnumerable<Movie>, IEnumerable<MovieViewModel>>(movies);
                response = request.CreateResponse(HttpStatusCode.OK, moviesVM);
                var response_Content = response.Content;

                return response;
            });
        }

        [Route("details/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var movie = moviesRepository.GetSingle(id);

                HttpResponseMessage response = null;

                //var config = new MapperConfiguration(cfg =>
                //{
                //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //    cfg.CreateMap<Movie, MovieViewModel>();
                //});
                //MovieViewModel movieVM = config.CreateMapper().Map<Movie, MovieViewModel>(movie);
                //response = request.CreateResponse(HttpStatusCode.OK, movieVM);

                //--
                MovieViewModel movieVM = Mapper.Map<Movie, MovieViewModel>(movie);
                response = request.CreateResponse(HttpStatusCode.OK, movieVM);
                //--

                return response;
            });
        }

        [AllowAnonymous]
        [Route("{page:int=0}/{pageSize=3}/{filter?}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, () =>
            {
                List<Movie> movies = null;
                int totalMovies = new int();

                if (!string.IsNullOrEmpty(filter))
                {
                    movies = moviesRepository
                        .FindBy(m => m.Title.ToLower()
                        .Contains(filter.ToLower().Trim()))
                        .OrderBy(m => m.ID)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .ToList();

                    totalMovies = moviesRepository
                        .FindBy(m => m.Title.ToLower()
                        .Contains(filter.ToLower().Trim()))
                        .Count();
                }
                else
                {
                    movies = moviesRepository
                        .GetAll()
                        .OrderBy(m => m.ID)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .ToList();

                    totalMovies = moviesRepository.GetAll().Count();
                }

                HttpResponseMessage response = null;

                //var config = new MapperConfiguration(cfg =>
                //{
                //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //    cfg.CreateMap<Movie, MovieViewModel>();
                //});
                //IEnumerable<MovieViewModel> moviesVM = config.CreateMapper().Map<IEnumerable<Movie>, IEnumerable<MovieViewModel>>(movies);

                //--
                IEnumerable<MovieViewModel> moviesVM = Mapper.Map<IEnumerable<Movie>, IEnumerable<MovieViewModel>>(movies);
                //--

                PaginationSet<MovieViewModel> pagedSet = new PaginationSet<MovieViewModel>()
                {
                    Page = currentPage,
                    TotalCount = totalMovies,
                    TotalPages = (int)Math.Ceiling((decimal)totalMovies / currentPageSize),
                    Items = moviesVM
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);


                return response;
            });
        }

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER")]
        public HttpResponseMessage Add(HttpRequestMessage request, MovieViewModel movie)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    Movie newMovie = new Movie();
                    newMovie.UpdateMovie(movie);

                    for (int i = 0; i < movie.NumberOfStocks; i++)
                    {
                        Stock stock = new Stock()
                        {
                            IsAvailable = true,
                            Movie = newMovie,
                            UniqueKey = Guid.NewGuid()
                        };
                        newMovie.Stocks.Add(stock);
                    }

                    moviesRepository.Add(newMovie);

                    unitOfWork.Commit();

                    //// Update view model
                    //var config = new MapperConfiguration(cfg =>
                    //{
                    //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                    //    cfg.CreateMap<Movie, MovieViewModel>();
                    //});
                    //movie = config.CreateMapper().Map<Movie, MovieViewModel>(newMovie);

                    movie = Mapper.Map<Movie, MovieViewModel>(newMovie);
                    response = request.CreateResponse(HttpStatusCode.Created, movie);
                }

                return response;

            });
        }

        [HttpPost]
        [Route("update")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN")]
        public HttpResponseMessage Update(HttpRequestMessage request, MovieViewModel movie)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var movieDb = moviesRepository.GetSingle(movie.ID);
                    if (movieDb == null)
                        response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid movie.");
                    else
                    {
                        movieDb.UpdateMovie(movie);
                        movie.Image = movieDb.Image;
                        moviesRepository.Edit(movieDb);

                        unitOfWork.Commit();
                        response = request.CreateResponse(HttpStatusCode.OK, movie);
                    }
                }

                return response;
            });
        }

        [MimeMultipart]
        [Route("images/upload")]
        public HttpResponseMessage Post(HttpRequestMessage request, int movieId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var movieOld = moviesRepository.GetSingle(movieId);
                if (movieOld == null)
                    response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid movie.");
                else
                {
                    var uploadPath = HttpContext.Current.Server.MapPath("~/Content/images/movies");

                    var multipartFormDataStreamProvider = new UploadMultipartFormProvider(uploadPath);

                    // Read the MIME multipart asynchronously
                    Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                    string _localFileName = multipartFormDataStreamProvider
                        .FileData.Select(multiPartData => multiPartData.LocalFileName).FirstOrDefault();

                    // Create response
                    FileUploadResult fileUploadResult = new FileUploadResult
                    {
                        LocalFilePath = _localFileName,

                        FileName = Path.GetFileName(_localFileName),

                        FileLength = new FileInfo(_localFileName).Length
                    };

                    // update database
                    movieOld.Image = fileUploadResult.FileName;
                    moviesRepository.Edit(movieOld);
                    unitOfWork.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK, fileUploadResult);
                }

                return response;
            });
        }
    }
}