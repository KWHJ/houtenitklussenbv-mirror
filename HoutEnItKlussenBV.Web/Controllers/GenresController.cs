﻿using AutoMapper;
using HomeCinema.Data.Infrastructure;
using HomeCinema.Data.Repositories;
using HomeCinema.Entities;
using HomeCinema.Web.Infrastructure.Core;
using HomeCinema.Web.Mappings;
using HomeCinema.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HomeCinema.Web.Controllers
{
    [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER, BASIS_USER")]
    [RoutePrefix("api/genres")]
    public class GenresController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<Genre> genresRepository;

        public GenresController(IEntityBaseRepository<Genre> genresRepository,
             IEntityBaseRepository<Error> _errorsRepository, IUnitOfWork _unitOfWork)
            : base(_errorsRepository, _unitOfWork)
        {
            this.genresRepository = genresRepository;
        }

        [AllowAnonymous]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            HttpResponseMessage response = null;

            return CreateHttpResponse(request, () =>
            {
                var genres = genresRepository.GetAll().ToList();

                //--TEST
                HttpResponseMessage response_TEST = null;
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<DomainToViewModelMappingProfile>();
                    cfg.CreateMap<Genre, GenreViewModel>();
                });
                IEnumerable<GenreViewModel> genresVM_TEST = config.CreateMapper().Map<IEnumerable<Genre>, IEnumerable<GenreViewModel>>(genres);
                response_TEST = request.CreateResponse(HttpStatusCode.OK, genresVM_TEST);

                var response_TEST_Content = response_TEST.Content;
                //--TEST

                IEnumerable<GenreViewModel> genresVM = Mapper.Map<IEnumerable<Genre>, IEnumerable<GenreViewModel>>(genres);
                response = request.CreateResponse(HttpStatusCode.OK, genresVM);

                var response_Content = response.Content;

                return response;
            });
        }
    }
}
