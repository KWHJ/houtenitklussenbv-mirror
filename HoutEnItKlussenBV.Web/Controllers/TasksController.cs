﻿namespace HoutEnItKlussenBV.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using System.Web.Http;
    using AutoMapper;
    using Data.Infrastructure;
    using Data.Repositories;
    using Entities;
    using Infrastructure.Core;
    using Models;
    using System.Collections.ObjectModel;

    //[Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER, BASIS_USER")]
    [RoutePrefix("api/tasks")]
    public class TasksController : ApiControllerBase
    {
        //private readonly IEntityBaseRepository<Address> addressesRepository;
        //private readonly IEntityBaseRepository<Barcode> barcodesRepository;
        //private readonly IEntityBaseRepository<Customer> customersRepository;
        private readonly IEntityBaseRepository<Image> imagesRepository;
        //private readonly IEntityBaseRepository<Part> partsRepository;
        //private readonly IEntityBaseRepository<PartType> parttypesRepository;
        //private readonly IEntityBaseRepository<Project> projectsRepository;
        //private readonly IEntityBaseRepository<ProjectTask> projecttasksRepository;
        //private readonly IEntityBaseRepository<Stock> stocksRepository;
        //private readonly IEntityBaseRepository<StockType> stocktypesRepository;
        private readonly IEntityBaseRepository<Task> tasksRepository;
        private readonly IEntityBaseRepository<TaskImage> taskimagesRepository;
        //private readonly IEntityBaseRepository<TaskPart> taskpartsRepository;
        //private readonly IEntityBaseRepository<TaskType> tasktypesRepository;

        public TasksController(
            //IEntityBaseRepository<Address> addressesRepository,
            //IEntityBaseRepository<Barcode> barcodesRepository,
            //IEntityBaseRepository<Customer> customersRepository,
            IEntityBaseRepository<Image> imagesRepository,
            //IEntityBaseRepository<Part> partsRepository,
            //IEntityBaseRepository<PartType> parttypesRepository,
            //IEntityBaseRepository<Project> projectsRepository,
            //IEntityBaseRepository<ProjectTask> projecttasksRepository,
            //IEntityBaseRepository<Stock> stocksRepository,
            //IEntityBaseRepository<StockType> stocktypesRepository,
            IEntityBaseRepository<Task> tasksRepository,
            IEntityBaseRepository<TaskImage> taskimagesRepository,
            //IEntityBaseRepository<TaskPart> taskpartsRepository,
            //IEntityBaseRepository<TaskType> tasktypesRepository,

            IEntityBaseRepository<Error> errorsRepository,
            IUnitOfWork unitOfWork) : base(errorsRepository, unitOfWork)
        {
            //this.addressesRepository = addressesRepository;
            //this.barcodesRepository = barcodesRepository;
            //this.customersRepository = customersRepository;
            this.imagesRepository = imagesRepository;
            //this.partsRepository = partsRepository;
            //this.parttypesRepository = parttypesRepository;
            //this.projectsRepository = projectsRepository;
            //this.projecttasksRepository = projecttasksRepository;
            //this.stocksRepository = stocksRepository;
            //this.stocktypesRepository = stocktypesRepository;
            this.tasksRepository = tasksRepository;
            this.taskimagesRepository = taskimagesRepository;
            //this.taskpartsRepository = taskpartsRepository;
            //this.tasktypesRepository = tasktypesRepository;

        }

        [AllowAnonymous]
        [Route("latest")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request,
                () =>
                {
                    HttpResponseMessage response = null;

                    #region DB Consistency Check
                    //var test_address = addressesRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_barcode = barcodesRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_customer = customersRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    var test_image = imagesRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_part = partsRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_parttype = parttypesRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_project = projectsRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_projecttask = projecttasksRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_stock = stocksRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_stocktype = stocktypesRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_task = tasksRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_taskimage = taskimagesRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_taskpart = taskpartsRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    //var test_tasktype = tasktypesRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(1).ToList();
                    #endregion

                    List<Task> derivedtasks = new List<Task>();

                    var tasks = tasksRepository.GetAll().OrderByDescending(m => m.CreationTimeStamp).Take(2).ToList();
                    foreach (var task in tasks)
                    {
                        Task derivedtask = new Task();
                        derivedtask.TaskImages = new Collection<TaskImage>()
                        {
                            //new TaskImage() { }
                        };

                        derivedtask.ImageNames = new Dictionary<int, Dictionary<string, string>>() { };

                        derivedtask.Id = task.Id;
                        derivedtask.Name = task.Name;
                        derivedtask.Description = task.Description;

                        foreach (var taskimage in taskimagesRepository.FindBy(m => m.TaskId == task.Id).ToList())
                        {
                            var image = imagesRepository.GetSingle(taskimage.ImageId);
                            taskimage.Image = image;

                            if (taskimage.TaskId == task.Id)
                            {
                                //derivedtask.TaskImages.Add(taskimage);

                                Dictionary<string, string> mydict = new Dictionary<string, string>();
                                mydict.Add("Name", taskimage.Image.Name);
                                mydict.Add("Description", taskimage.Image.Description);
                                mydict.Add("LocationUrl", taskimage.Image.LocationUrl);
                                mydict.Add("Task", task.Name);

                                derivedtask.ImageNames.Add(taskimage.Id, mydict);

                            }

                            task.TaskImages.Add(taskimage);

                        }

                        derivedtasks.Add(derivedtask);
                    }

                    //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM
                    //////HttpResponseMessage response_TEST = null;
                    //////var config = new MapperConfiguration(cfg =>
                    //////{
                    //////    cfg.AddProfile<DomainToViewModelMappingProfile>();
                    //////    cfg.CreateMap<Task, TaskViewModel>();
                    //////});
                    //////IEnumerable<TaskViewModel> tasksVM_TEST = config.CreateMapper().Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
                    //////response_TEST = request.CreateResponse(HttpStatusCode.OK, tasksVM_TEST);
                    //////var response_TEST_Content = response_TEST.Content;
                    //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM


                    //IEnumerable<TaskViewModel> tasksVM = Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
                    IEnumerable<TaskViewModel> tasksVM = Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(derivedtasks);
                    response = request.CreateResponse(HttpStatusCode.OK, tasksVM);
                    var response_Content = response.Content;

                    return response;
                });
        }

        [Route("details/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var task = tasksRepository.GetSingle(id);

                HttpResponseMessage response = null;

                //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM
                //////var config = new MapperConfiguration(cfg =>
                //////{
                //////    cfg.AddProfile<DomainToViewModelMappingProfile>();
                //////    cfg.CreateMap<Task, TaskViewModel>();
                //////});
                //////TaskViewModel taskVM = config.CreateMapper().Map<Task, TaskViewModel>(task);
                //////response = request.CreateResponse(HttpStatusCode.OK, taskVM);
                //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM

                TaskViewModel taskVM = Mapper.Map<Task, TaskViewModel>(task);
                response = request.CreateResponse(HttpStatusCode.OK, taskVM);

                return response;
            });
        }

        [AllowAnonymous]
        [Route("{page:int=0}/{pageSize=3}/{filter?}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request,
                () =>
                {
                    List<Task> tasks = null;
                    int totalTasks = new int();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        tasks = tasksRepository
                            .FindBy(m => m.Name.ToLower()
                            .Contains(filter.ToLower().Trim()))
                            .OrderBy(m => m.Id)
                            .Skip(currentPage * currentPageSize)
                            .Take(currentPageSize)
                            .ToList();

                        totalTasks = tasksRepository
                            .FindBy(m => m.Name.ToLower()
                            .Contains(filter.ToLower().Trim()))
                            .Count();
                    }
                    else
                    {
                        tasks = tasksRepository
                            .GetAll()
                            .OrderBy(m => m.Id)
                            .Skip(currentPage * currentPageSize)
                            .Take(currentPageSize)
                            .ToList();

                        totalTasks = tasksRepository.GetAll().Count();
                    }

                    HttpResponseMessage response = null;

                    //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM
                    //////var config = new MapperConfiguration(cfg =>
                    //////{
                    //////    cfg.AddProfile<DomainToViewModelMappingProfile>();
                    //////    cfg.CreateMap<Task, TaskViewModel>();
                    //////});
                    //////IEnumerable<TaskViewModel> tasksVM = config.CreateMapper().Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
                    //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM

                    IEnumerable<TaskViewModel> tasksVM = Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);

                    PaginationSet<TaskViewModel> pagedSet = new PaginationSet<TaskViewModel>()
                    {
                        Page = currentPage,
                        TotalCount = totalTasks,
                        TotalPages = (int)Math.Ceiling((decimal)totalTasks / currentPageSize),
                        Items = tasksVM
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                    return response;
                });
        }

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER")]
        public HttpResponseMessage Add(HttpRequestMessage request, TaskViewModel task)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    Task newTask = new Task();

                    //TODO CHANGE
                    //newTask.UpdateTask(task);

                    //for (int i = 0; i < task.NumberOfStocks; i++)
                    //{
                    //    Stock stock = new Stock()
                    //    {
                    //        IsAvailable = true,
                    //        Task = newTask,
                    //        UniqueKey = Guid.NewGuid()
                    //    };
                    //    newTask.Stocks.Add(stock);
                    //}

                    tasksRepository.Add(newTask);

                    unitOfWork.Commit();

                    //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM
                    //////// Update view model
                    //////var config = new MapperConfiguration(cfg =>
                    //////{
                    //////    cfg.AddProfile<DomainToViewModelMappingProfile>();
                    //////    cfg.CreateMap<Task, TaskViewModel>();
                    //////});
                    //////task = config.CreateMapper().Map<Task, TaskViewModel>(newTask);
                    //////LEAVE IT AUTOMAPPER BUG FALSE POSITIVE ON OBSOLETISM

                    task = Mapper.Map<Task, TaskViewModel>(newTask);
                    response = request.CreateResponse(HttpStatusCode.Created, task);
                }

                return response;

            });
        }

        [HttpPost]
        [Route("update")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN")]
        public HttpResponseMessage Update(HttpRequestMessage request, TaskViewModel task)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var taskDb = tasksRepository.GetSingle(task.Id);
                    if (taskDb == null)
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid task.");
                    }
                    //TODO CHANGE
                    //else
                    //{
                    //    taskDb.UpdateTask(task);
                    //    task.Image = taskDb.Image;
                    //    tasksRepository.Edit(taskDb);

                    //    unitOfWork.Commit();
                    //    response = request.CreateResponse(HttpStatusCode.OK, task);
                    //}
                }

                return response;
            });
        }

        [MimeMultipart]
        [Route("images/upload")]
        public HttpResponseMessage Post(HttpRequestMessage request, int taskId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var taskOld = tasksRepository.GetSingle(taskId);
                if (taskOld == null)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid task.");
                }
                else
                {
                    var uploadPath = HttpContext.Current.Server.MapPath("~/Content/images/tasks");

                    var multipartFormDataStreamProvider = new UploadMultipartFormProvider(uploadPath);

                    // Read the MIME multipart asynchronously
                    Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                    string _localFileName = multipartFormDataStreamProvider
                        .FileData.Select(multiPartData => multiPartData.LocalFileName).FirstOrDefault();

                    // Create response
                    FileUploadResult fileUploadResult = new FileUploadResult
                    {
                        LocalFilePath = _localFileName,
                        FileName = Path.GetFileName(_localFileName),
                        FileLength = new FileInfo(_localFileName).Length
                    };

                    // update database
                    taskOld.LastChangeTimeStamp = DateTime.Now;
                    tasksRepository.Edit(taskOld);
                    unitOfWork.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK, fileUploadResult);
                }

                return response;
            });
        }
    }
}