﻿namespace HomeCinema.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AutoMapper;
    using Data.Extensions;
    using Data.Infrastructure;
    using Data.Repositories;
    using Entities;
    using Infrastructure.Core;
    using Infrastructure.Extensions;
    using Mappings;
    using Models;

    [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER, BASIS_USER")]
    [RoutePrefix("api/customers")]
    public class CustomersController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<Customer> customersRepository;

        public CustomersController(
            IEntityBaseRepository<Customer> customersRepository,
            IEntityBaseRepository<Error> errorsRepository,
            IUnitOfWork unitOfWork) : base(errorsRepository, unitOfWork)
        {
            this.customersRepository = customersRepository;
        }

        public HttpResponseMessage Get(HttpRequestMessage request, string filter)
        {
            filter = filter.ToLower().Trim();

            return CreateHttpResponse(
                request,
                () =>
                {
                    var customers = customersRepository.GetAll()
                        .Where(c => c.Email.ToLower().Contains(filter) ||
                        c.FirstName.ToLower().Contains(filter) ||
                        c.LastName.ToLower().Contains(filter)).ToList();

                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile<DomainToViewModelMappingProfile>();
                        cfg.CreateMap<Customer, CustomerViewModel>();
                    });

                    HttpResponseMessage response = null;
                    //var customersVm = config.CreateMapper().Map<IEnumerable<Customer>, IEnumerable<CustomerViewModel>>(customers);
                    //response = request.CreateResponse(HttpStatusCode.OK, customersVm);
                    //return response;

                    //--
                    var customersVm = Mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerViewModel>>(customers);
                    response = request.CreateResponse(HttpStatusCode.OK, customersVm);
                    //--

                    return response;
                });
            }

        [Route("details/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    var customer = customersRepository.GetSingle(id);

                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile<DomainToViewModelMappingProfile>();
                        cfg.CreateMap<Customer, CustomerViewModel>();
                    });

                    HttpResponseMessage response = null;

                    //CustomerViewModel customerVm = config.CreateMapper().Map<Customer, CustomerViewModel>(customer);
                    //response = request.CreateResponse(HttpStatusCode.OK, customerVm);
                    //return response;

                    //--
                    CustomerViewModel customerVm = Mapper.Map<Customer, CustomerViewModel>(customer);
                    response = request.CreateResponse(HttpStatusCode.OK, customerVm);
                    //--

                    return response;
                });
        }

        [HttpPost]
        [Route("register")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER")]
        public HttpResponseMessage Register(HttpRequestMessage request, CustomerViewModel customer)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;

                    string[] modelstateErrorMessages;
                    if (!ModelState.IsValid)
                    {
                        modelstateErrorMessages = ModelState.Keys.SelectMany(k => ModelState[k].Errors).Select(m => m.ErrorMessage).ToArray();
                        response = request.CreateResponse(HttpStatusCode.BadRequest, modelstateErrorMessages);
                    }
                    else
                    {
                        if (customersRepository.UserExists(customer.Email, customer.IdentityCard))
                        {
                            ModelState.AddModelError("Invalid user", "Email or Identity Card number already exists");
                            modelstateErrorMessages = ModelState.Keys.SelectMany(k => ModelState[k].Errors).Select(m => m.ErrorMessage).ToArray();
                            response = request.CreateResponse(HttpStatusCode.BadRequest, modelstateErrorMessages);
                        }
                        else
                        {
                            Customer newCustomer = new Customer();
                            newCustomer.UpdateCustomer(customer);
                            customersRepository.Add(newCustomer);

                            unitOfWork.Commit();

                            //Update view model
                            //var config = new MapperConfiguration(cfg =>
                            //{
                            //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                            //    cfg.CreateMap<Customer, CustomerViewModel>();
                            //});
                            //customer = config.CreateMapper().Map<Customer, CustomerViewModel>(newCustomer);

                            customer = Mapper.Map<Customer, CustomerViewModel>(newCustomer);

                            response = request.CreateResponse(HttpStatusCode.Created, customer);
                        }
                    }

                    return response;
                });
        }

        [HttpPost]
        [Route("update")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN")]
        public HttpResponseMessage Update(HttpRequestMessage request, CustomerViewModel customerVM)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;

                    string[] modelstateErrorMessages;
                    if (!ModelState.IsValid)
                    {
                        modelstateErrorMessages = ModelState.Keys.SelectMany(k => ModelState[k].Errors).Select(m => m.ErrorMessage).ToArray();
                        response = request.CreateResponse(HttpStatusCode.BadRequest, modelstateErrorMessages);
                    }
                    else
                    {
                        Customer _customer = customersRepository.GetSingle(customerVM.ID);
                        _customer.UpdateCustomer(customerVM);

                        unitOfWork.Commit();

                        response = request.CreateResponse(HttpStatusCode.OK);
                    }

                    return response;
                });
        }

        [HttpGet]
        [Route("search/{page:int=0}/{pageSize=4}/{filter?}")]
        public HttpResponseMessage Search(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;
                    List<Customer> customers = null;
                    int totalCustomers = new int();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        filter = filter.Trim().ToLower();

                        customers = customersRepository
                            .FindBy(c => c.LastName.ToLower().Contains(filter) ||
                                    c.IdentityCard.ToLower().Contains(filter) ||
                                    c.FirstName.ToLower().Contains(filter))
                            .OrderBy(c => c.ID)
                            .Skip(currentPage * currentPageSize)
                            .Take(currentPageSize)
                            .ToList();

                        totalCustomers = customersRepository
                            .GetAll()
                            .Where(c => c.LastName.ToLower().Contains(filter) ||
                                    c.IdentityCard.ToLower().Contains(filter) ||
                                    c.FirstName.ToLower().Contains(filter))
                            .Count();
                    }
                    else
                    {
                        customers = customersRepository.GetAll()
                            .OrderBy(c => c.ID)
                            .Skip(currentPage * currentPageSize)
                            .Take(currentPageSize)
                        .ToList();

                        totalCustomers = customersRepository.GetAll().Count();
                    }

                    //var config = new MapperConfiguration(cfg =>
                    //{
                    //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                    //    cfg.CreateMap<Customer, CustomerViewModel>();
                    //});
                    //IEnumerable<CustomerViewModel> customersVM = config.CreateMapper().Map<IEnumerable<Customer>, IEnumerable<CustomerViewModel>>(customers);

                    IEnumerable<CustomerViewModel> customersVM = Mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerViewModel>>(customers);
                    PaginationSet<CustomerViewModel> pagedSet = new PaginationSet<CustomerViewModel>()
                    {
                        Page = currentPage,
                        TotalCount = totalCustomers,
                        TotalPages = (int)Math.Ceiling((decimal)totalCustomers / currentPageSize),
                        Items = customersVM
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                    return response;
                });
        }
    }
}
