﻿namespace HomeCinema.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AutoMapper;
    using Data.Extensions;
    using Data.Infrastructure;
    using Data.Repositories;
    using Entities;
    using Infrastructure.Core;
    using Infrastructure.Extensions;
    using Mappings;
    using Models;

    [Authorize(Roles = "SUPER_ADMIN")]
    [RoutePrefix("api/users")]
    public class UsersController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<User> usersRepository;

        public UsersController(
            IEntityBaseRepository<User> usersRepository,
            IEntityBaseRepository<Error> errorsRepository,
            IUnitOfWork unitOfWork) : base(errorsRepository, unitOfWork)
        {
            this.usersRepository = usersRepository;
        }

        public HttpResponseMessage Get(HttpRequestMessage request, string filter)
        {
            filter = filter.ToLower().Trim();

            return CreateHttpResponse(
                request,
                () =>
                {
                    var users = usersRepository.GetAll()
                        .Where(c => c.Email.ToLower().Contains(filter)
                            //|| c.FirstName.ToLower().Contains(filter)
                            //|| c.LastName.ToLower().Contains(filter)
                            )
                        .ToList();

                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile<DomainToViewModelMappingProfile>();
                        cfg.CreateMap<User, UserViewModel>();
                    });

                    HttpResponseMessage response = null;

                    //var usersVm = config.CreateMapper().Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);
                    //response = request.CreateResponse(HttpStatusCode.OK, usersVm);
                    //return response;

                    //--
                    var usersVm = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);
                    response = request.CreateResponse(HttpStatusCode.OK, usersVm);
                    //--

                    return response;
                });
        }

        [Route("details/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    var user = usersRepository.GetSingle(id);

                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile<DomainToViewModelMappingProfile>();
                        cfg.CreateMap<User, UserViewModel>();
                    });

                    HttpResponseMessage response = null;

                    //UserViewModel userVm = config.CreateMapper().Map<User, UserViewModel>(user);
                    //response = request.CreateResponse(HttpStatusCode.OK, userVm);
                    //return response;

                    //--
                    UserViewModel userVm = Mapper.Map<User, UserViewModel>(user);
                    response = request.CreateResponse(HttpStatusCode.OK, userVm);
                    //--

                    return response;
                });
        }

        [HttpPost]
        [Route("register")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN, ELEVATED_USER")]
        public HttpResponseMessage Register(HttpRequestMessage request, UserViewModel user)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;

                    string[] modelstateErrorMessages;
                    if (!ModelState.IsValid)
                    {
                         modelstateErrorMessages = ModelState.Keys.SelectMany(k => ModelState[k].Errors).Select(m => m.ErrorMessage).ToArray();
                        response = request.CreateResponse(HttpStatusCode.BadRequest, modelstateErrorMessages);
                    }
                    else
                    {
                        if (usersRepository.UserExists(user.Email
                                                    //, user.IdentityCard
                                                        ) )
                        {
                             modelstateErrorMessages = ModelState.Keys.SelectMany(k => ModelState[k].Errors).Select(m => m.ErrorMessage).ToArray();
                            ModelState.AddModelError("Invalid user", "Email or Identity Card number already exists");
                            response = request.CreateResponse(HttpStatusCode.BadRequest, modelstateErrorMessages);
                        }
                        else
                        {
                            User newUser = new User();
                            newUser.UpdateUser(user);
                            usersRepository.Add(newUser);

                            unitOfWork.Commit();

                            //Update view model
                            //var config = new MapperConfiguration(cfg =>
                            //{
                            //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                            //    cfg.CreateMap<User, UserViewModel>();
                            //});
                            //user = config.CreateMapper().Map<User, UserViewModel>(newUser);

                            user = Mapper.Map<User, UserViewModel>(newUser);

                            response = request.CreateResponse(HttpStatusCode.Created, user);
                        }
                    }

                    return response;
                });
        }

        [HttpPost]
        [Route("update")]
        [Authorize(Roles = "SUPER_ADMIN, BASIC_ADMIN")]
        public HttpResponseMessage Update(HttpRequestMessage request, UserViewModel userVM)
        {
            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;

                    string[] modelstateErrorMessages;
                    if (!ModelState.IsValid)
                    {
                        modelstateErrorMessages = ModelState.Keys.SelectMany(k => ModelState[k].Errors).Select(m => m.ErrorMessage).ToArray();
                        response = request.CreateResponse(HttpStatusCode.BadRequest, modelstateErrorMessages);
                    }
                    else
                    {
                        User _user = usersRepository.GetSingle(userVM.ID);
                        _user.UpdateUser(userVM);

                        unitOfWork.Commit();

                        response = request.CreateResponse(HttpStatusCode.OK);
                    }

                    return response;
                });
        }

        [HttpGet]
        [Route("search/{page:int=0}/{pageSize=4}/{filter?}")]
        public HttpResponseMessage Search(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(
                request,
                () =>
                {
                    HttpResponseMessage response = null;
                    List<User> users = null;
                    int totalUsers = new int();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        filter = filter.Trim().ToLower();

                        users = usersRepository
                            .FindBy(
                                    c =>
                                    // c.LastName.ToLower().Contains(filter)
                                    //|| c.IdentityCard.ToLower().Contains(filter)
                                    //|| c.FirstName.ToLower().Contains(filter)
                                    //||
                                    c.Email.ToLower().Contains(filter)
                                    )
                            .OrderBy(c => c.ID)
                            .Skip(currentPage * currentPageSize)
                            .Take(currentPageSize)
                            .ToList();

                        totalUsers = usersRepository
                            .GetAll()
                            .Where(c =>
                                    // c.LastName.ToLower().Contains(filter)
                                    //|| c.IdentityCard.ToLower().Contains(filter)
                                    //|| c.FirstName.ToLower().Contains(filter)
                                    //||
                                    c.Email.ToLower().Contains(filter)
                                    )
                            .Count();
                    }
                    else
                    {
                        users = usersRepository.GetAll()
                            .OrderBy(c => c.ID)
                            .Skip(currentPage * currentPageSize)
                            .Take(currentPageSize)
                        .ToList();

                        totalUsers = usersRepository.GetAll().Count();
                    }

                    //var config = new MapperConfiguration(cfg =>
                    //{
                    //    cfg.AddProfile<DomainToViewModelMappingProfile>();
                    //    cfg.CreateMap<User, UserViewModel>();
                    //});
                    //IEnumerable<UserViewModel> usersVM = config.CreateMapper().Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);

                    IEnumerable<UserViewModel> usersVM = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);
                    PaginationSet<UserViewModel> pagedSet = new PaginationSet<UserViewModel>()
                    {
                        Page = currentPage,
                        TotalCount = totalUsers,
                        TotalPages = (int)Math.Ceiling((decimal)totalUsers / currentPageSize),
                        Items = usersVM
                    };
                    response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                    return response;
                });
        }
    }
}
