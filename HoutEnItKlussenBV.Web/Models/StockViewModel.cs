﻿namespace HoutEnItKlussenBV.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using Infrastructure.Validators;

    public class StockViewModel : IValidatableObject
    {
        public int Id { get; set; }

        public int PartId { get; set; }

        public bool IsAvailable { get; set; }

        public int StockTypeId { get; set; }

        public int NumberInStock { get; set; }

        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp{ get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new StockViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}