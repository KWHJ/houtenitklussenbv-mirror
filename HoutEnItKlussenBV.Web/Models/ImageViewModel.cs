﻿namespace HoutEnItKlussenBV.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Infrastructure.Validators;
    using Entities;

    public class ImageViewModel : IValidatableObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string LocationUrl { get; set; }

        public List<TaskImage> TaskImages { get; set; }

        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new ImageViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}