﻿namespace HoutEnItKlussenBV.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using Infrastructure.Validators;
    using Entities;

    //[Bind(Exclude = "Image")]
    public class TaskViewModel : IValidatableObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<TaskImage> TaskImages { get; set; }

        public Dictionary<int, Dictionary<string, string> > ImageNames { get; set;}

        #region Currently not needed
        //public int TaskTypeId { get; set; }
        #endregion

        #region Probably never needed on page
        //public int LastChangeUserId { get; set; }

        //public DateTime CreationTimeStamp { get; set; }

        //public DateTime LastChangeTimeStamp { get; set; }
        #endregion

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new TaskViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}