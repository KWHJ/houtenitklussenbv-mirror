﻿namespace HoutEnItKlussenBV.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory interfaceDatabFactory;
        private HoutEnItKlussenBVContext ctx;

        public UnitOfWork(IDbFactory interfaceDatabaseFactory)
        {
            interfaceDatabFactory = interfaceDatabaseFactory;
        }

        public HoutEnItKlussenBVContext DbContext
        {
            get { return ctx ?? (ctx = interfaceDatabFactory.Init()); }
        }

        public void Commit()
        {
            DbContext.Commit();
        }
    }
}
