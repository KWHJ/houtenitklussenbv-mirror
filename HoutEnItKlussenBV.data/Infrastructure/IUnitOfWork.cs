﻿namespace HoutEnItKlussenBV.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
