﻿namespace HoutEnItKlussenBV.Data.Infrastructure
{
    using Data;
    using System;

    public interface IDbFactory : IDisposable
    {
        HoutEnItKlussenBVContext Init();
    }
}