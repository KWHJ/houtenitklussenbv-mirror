﻿namespace HoutEnItKlussenBV.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        HoutEnItKlussenBVContext ctx = new HoutEnItKlussenBVContext();

        public HoutEnItKlussenBVContext Init()
        {
            /*
             Without line
             > ctx.Configuration.ProxyCreationEnabled = false;
            The
            'System.Data.Entity.DynamicProxies.TaskImage_8B6392B7E707C0196F507D682FC61E1215E5CFDF085D3E636AF7B0829E2AE8AA'
            is a runtime EF generated version of a proxy to what would normally be considered a POCO object.
            Entity Framework has created this object because it tracks for instance when the object has changed, e.g.
            To Prevent EF from returning this object you have two choices (ATM):
            First:
            Try turning off Proxy object creation on your DbContext thru code line: DbContext.Configuration.ProxyCreationEnabled = false;
            Result: This will completely disable the create of proxy objects for every query to the specific DbContext.
            Note: This does not affect the cached object in the ObjectContext.
            Secondly
            Use EntityFramework 5.0+ with AsNoTracking().
            Note: ProxyCreationEnabled is still available in EF 5.0 as
            You should also be able to

            DbContext.Persons.AsNoTracking().FirstOrDefault();
            */

            //ctx.Configuration.ProxyCreationEnabled = false;


            if (ctx == null)
            {
                return new HoutEnItKlussenBVContext();
            }
            else
            {
                return ctx;
            }

            //return ctx ?? (ctx = new HoutEnItKlussenBVContext());
        }

        protected override void DisposeCore()
        {
            if (ctx != null)
            {
                ctx.Dispose();
            }
        }
    }
}
