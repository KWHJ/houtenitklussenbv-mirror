namespace HoutEnItKlussenBV.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DefaultSchema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImageTask",
                c => new
                    {
                        Image_Id = c.Int(nullable: false),
                        Task_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Image_Id, t.Task_Id })
                .ForeignKey("dbo.Image", t => t.Image_Id, cascadeDelete: true)
                .ForeignKey("dbo.Task", t => t.Task_Id, cascadeDelete: true)
                .Index(t => t.Image_Id)
                .Index(t => t.Task_Id);
            
            AddColumn("dbo.Project", "Task_Id", c => c.Int());
            CreateIndex("dbo.Project", "Task_Id");
            AddForeignKey("dbo.Project", "Task_Id", "dbo.Task", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Project", "Task_Id", "dbo.Task");
            DropForeignKey("dbo.ImageTask", "Task_Id", "dbo.Task");
            DropForeignKey("dbo.ImageTask", "Image_Id", "dbo.Image");
            DropIndex("dbo.ImageTask", new[] { "Task_Id" });
            DropIndex("dbo.ImageTask", new[] { "Image_Id" });
            DropIndex("dbo.Project", new[] { "Task_Id" });
            DropColumn("dbo.Project", "Task_Id");
            DropTable("dbo.ImageTask");
        }
    }
}
