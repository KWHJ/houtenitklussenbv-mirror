namespace HoutEnItKlussenBV.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Street = c.String(nullable: false, maxLength: 100),
                        Number = c.String(nullable: false, maxLength: 20),
                        NumberExt = c.String(),
                        Zipcode = c.String(nullable: false, maxLength: 20),
                        Town = c.String(nullable: false, maxLength: 50),
                        Province = c.String(maxLength: 100),
                        Country = c.String(maxLength: 100),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 200),
                        UniqueKey = c.Guid(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        Mobile = c.String(maxLength: 20),
                        Phone = c.String(maxLength: 20),
                        RegistrationDate = c.DateTime(nullable: false),
                        AddressId = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.AddressId, cascadeDelete: true)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.Project",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateReady = c.DateTime(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.ProjectTask",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        TaskId = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Project", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.Task", t => t.TaskId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.TaskId);
            
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        TaskTypeId = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskType", t => t.TaskTypeId, cascadeDelete: true)
                .Index(t => t.TaskTypeId);
            
            CreateTable(
                "dbo.TaskType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Barcode",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PartId = c.Int(nullable: false),
                        Code = c.String(),
                        CodeName = c.String(),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Error",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        StackTrace = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Image",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        LocationUrl = c.String(maxLength: 200),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Part",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        ImageId = c.Int(nullable: false),
                        PartTypeId = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PartType", t => t.PartTypeId, cascadeDelete: true)
                .Index(t => t.PartTypeId);
            
            CreateTable(
                "dbo.PartType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Stock",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PartId = c.Int(nullable: false),
                        IsAvailable = c.Boolean(nullable: false),
                        StockTypeId = c.Int(nullable: false),
                        NumberInStock = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Part", t => t.PartId, cascadeDelete: true)
                .ForeignKey("dbo.StockType", t => t.StockTypeId, cascadeDelete: true)
                .Index(t => t.PartId)
                .Index(t => t.StockTypeId);
            
            CreateTable(
                "dbo.StockType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsSingleStockItem = c.Boolean(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskPart",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.Int(nullable: false),
                        PartId = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Part", t => t.PartId, cascadeDelete: true)
                .ForeignKey("dbo.Task", t => t.TaskId, cascadeDelete: true)
                .Index(t => t.TaskId)
                .Index(t => t.PartId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 200),
                        HashedPassword = c.String(nullable: false, maxLength: 200),
                        Salt = c.String(nullable: false, maxLength: 200),
                        IsLocked = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskImage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.Int(nullable: false),
                        ImageId = c.Int(nullable: false),
                        LastChangeUserId = c.Int(nullable: false),
                        CreationTimeStamp = c.DateTime(nullable: false),
                        LastChangeTimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Image", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("dbo.Task", t => t.TaskId, cascadeDelete: true)
                .Index(t => t.TaskId)
                .Index(t => t.ImageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskImage", "TaskId", "dbo.Task");
            DropForeignKey("dbo.TaskImage", "ImageId", "dbo.Image");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.TaskPart", "TaskId", "dbo.Task");
            DropForeignKey("dbo.TaskPart", "PartId", "dbo.Part");
            DropForeignKey("dbo.Stock", "StockTypeId", "dbo.StockType");
            DropForeignKey("dbo.Stock", "PartId", "dbo.Part");
            DropForeignKey("dbo.Part", "PartTypeId", "dbo.PartType");
            DropForeignKey("dbo.ProjectTask", "TaskId", "dbo.Task");
            DropForeignKey("dbo.Task", "TaskTypeId", "dbo.TaskType");
            DropForeignKey("dbo.ProjectTask", "ProjectId", "dbo.Project");
            DropForeignKey("dbo.Project", "CustomerId", "dbo.Customer");
            DropForeignKey("dbo.Customer", "AddressId", "dbo.Address");
            DropIndex("dbo.TaskImage", new[] { "ImageId" });
            DropIndex("dbo.TaskImage", new[] { "TaskId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.TaskPart", new[] { "PartId" });
            DropIndex("dbo.TaskPart", new[] { "TaskId" });
            DropIndex("dbo.Stock", new[] { "StockTypeId" });
            DropIndex("dbo.Stock", new[] { "PartId" });
            DropIndex("dbo.Part", new[] { "PartTypeId" });
            DropIndex("dbo.Task", new[] { "TaskTypeId" });
            DropIndex("dbo.ProjectTask", new[] { "TaskId" });
            DropIndex("dbo.ProjectTask", new[] { "ProjectId" });
            DropIndex("dbo.Project", new[] { "CustomerId" });
            DropIndex("dbo.Customer", new[] { "AddressId" });
            DropTable("dbo.TaskImage");
            DropTable("dbo.User");
            DropTable("dbo.UserRole");
            DropTable("dbo.Role");
            DropTable("dbo.TaskPart");
            DropTable("dbo.StockType");
            DropTable("dbo.Stock");
            DropTable("dbo.PartType");
            DropTable("dbo.Part");
            DropTable("dbo.Image");
            DropTable("dbo.Error");
            DropTable("dbo.Barcode");
            DropTable("dbo.TaskType");
            DropTable("dbo.Task");
            DropTable("dbo.ProjectTask");
            DropTable("dbo.Project");
            DropTable("dbo.Customer");
            DropTable("dbo.Address");
        }
    }
}
