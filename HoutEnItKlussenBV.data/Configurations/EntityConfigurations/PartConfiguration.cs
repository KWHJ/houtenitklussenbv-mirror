namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class PartConfiguration : EntityBaseConfiguration<Part>
    {
        public PartConfiguration()
        {
            Property(i => i.Name).IsRequired().HasMaxLength(100);
            Property(i => i.Description).IsRequired().IsMaxLength();
            Property(i => i.ImageId).IsRequired();
            Property(i => i.PartTypeId).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
