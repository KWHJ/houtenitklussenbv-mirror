﻿namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class ImageConfiguration : EntityBaseConfiguration<Image>
    {
        public ImageConfiguration()
        {
            Property(i => i.Name).IsRequired().HasMaxLength(100);
            Property(i => i.Description).IsRequired().IsMaxLength();
            Property(i => i.LocationUrl).HasMaxLength(200);

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
