namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public partial class AddressConfiguration : EntityBaseConfiguration<Address>
    {
        public AddressConfiguration()
        {
            Property(i => i.Street).IsRequired().HasMaxLength(100);
            Property(i => i.Number).IsRequired().HasMaxLength(20);
            Property(i => i.NumberExt).IsOptional();
            Property(i => i.Zipcode).IsRequired().HasMaxLength(20);
            Property(i => i.Town).IsRequired().HasMaxLength(50);
            Property(i => i.Province).HasMaxLength(100);
            Property(i => i.Country).HasMaxLength(100);

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
