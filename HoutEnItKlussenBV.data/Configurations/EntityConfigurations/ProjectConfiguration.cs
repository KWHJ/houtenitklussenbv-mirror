namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class ProjectConfiguration : EntityBaseConfiguration<Project>
    {
        public ProjectConfiguration()
        {
            Property(i => i.Name).IsRequired().HasMaxLength(100);
            Property(i => i.Description).IsRequired().IsMaxLength();
            Property(i => i.CustomerId).IsRequired();
            Property(i => i.DateCreated).IsRequired();
            Property(i => i.DateReady).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
