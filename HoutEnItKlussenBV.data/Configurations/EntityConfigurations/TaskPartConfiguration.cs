namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class TaskPartConfiguration : EntityBaseConfiguration<TaskPart>
    {
        public TaskPartConfiguration()
        {
            Property(i => i.TaskId).IsRequired();
            Property(i => i.PartId).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
