namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class StockConfiguration : EntityBaseConfiguration<Stock>
    {
        public StockConfiguration()
        {
            Property(i => i.PartId).IsRequired();
            Property(i => i.IsAvailable).IsRequired();
            Property(i => i.StockTypeId).IsRequired();
            Property(i => i.NumberInStock).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
