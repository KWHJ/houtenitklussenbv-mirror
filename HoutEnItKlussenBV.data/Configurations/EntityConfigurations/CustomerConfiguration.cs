namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class CustomerConfiguration : EntityBaseConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            Property(i => i.FirstName).IsRequired().HasMaxLength(100);
            Property(i => i.LastName).IsRequired().HasMaxLength(100);
            Property(i => i.Email).IsRequired().HasMaxLength(200);
            Property(i => i.UniqueKey).IsRequired();
            Property(i => i.DateOfBirth).IsRequired();
            Property(i => i.Mobile).HasMaxLength(20);
            Property(i => i.Phone).HasMaxLength(20);
            Property(i => i.RegistrationDate).IsRequired();
            Property(i => i.AddressId).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
