namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class PartTypeConfiguration : EntityBaseConfiguration<PartType>
    {
        public PartTypeConfiguration()
        {
            Property(i => i.Name).IsRequired().HasMaxLength(100);
            Property(i => i.Description).IsRequired().IsMaxLength();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
