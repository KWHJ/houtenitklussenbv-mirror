namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class StockTypeConfiguration : EntityBaseConfiguration<StockType>
    {
        public StockTypeConfiguration()
        {
            Property(i => i.IsSingleStockItem).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
