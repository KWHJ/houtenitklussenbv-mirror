namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public partial class TaskTypeConfiguration : EntityBaseConfiguration<TaskType>
    {
        public TaskTypeConfiguration()
        {
            Property(i => i.Name).IsRequired().HasMaxLength(100);
            Property(i => i.Description).IsRequired().IsMaxLength();
            //Property(i => i.Image).IsOptional().HasMaxLength(100);

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
