namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class TaskConfiguration : EntityBaseConfiguration<Task>
    {
        public TaskConfiguration()
        {
            Property(i => i.Name).IsRequired().HasMaxLength(100);
            Property(i => i.Description).IsRequired().IsMaxLength();
            Property(i => i.TaskTypeId).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
