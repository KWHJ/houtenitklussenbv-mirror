namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class BarcodeConfiguration : EntityBaseConfiguration<Barcode>
    {
        public BarcodeConfiguration()
        {
            Property(i => i.PartId).IsRequired();
            Property(i => i.Code).IsOptional().HasMaxLength(7089);
            Property(i => i.CodeName).IsOptional();             // e.g. EAN, etc.

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
