namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class TaskImageConfiguration : EntityBaseConfiguration<TaskImage>
    {
        public TaskImageConfiguration()
        {
            Property(i => i.TaskId).IsRequired();
            Property(i => i.ImageId).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
