namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class ProjectTaskConfiguration : EntityBaseConfiguration<ProjectTask>
    {
        public ProjectTaskConfiguration()
        {
            Property(i => i.ProjectId).IsRequired();
            Property(i => i.TaskId).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
