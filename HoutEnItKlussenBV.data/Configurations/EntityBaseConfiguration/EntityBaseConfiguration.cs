﻿namespace HoutEnItKlussenBV.Data.Configurations
{
    using System.Data.Entity.ModelConfiguration;
    using Entities;

    public class EntityBaseConfiguration<T> : EntityTypeConfiguration<T> where T : class, IEntityBase
    {
        public EntityBaseConfiguration()
        {
            HasKey(e => e.Id);
        }
    }
}
