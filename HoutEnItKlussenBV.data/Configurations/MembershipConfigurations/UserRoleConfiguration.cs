namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class UserRoleConfiguration : EntityBaseConfiguration<UserRole>
    {
        public UserRoleConfiguration()
        {
            Property(i => i.UserId).IsRequired();
            Property(i => i.RoleId).IsRequired();

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();
        }
    }
}
