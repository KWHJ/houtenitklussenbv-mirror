namespace HoutEnItKlussenBV.Data.Configurations
{
    using Entities;

    public class UserConfiguration : EntityBaseConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(i => i.Username).IsRequired().HasMaxLength(100);
            Property(i => i.Email).IsRequired().HasMaxLength(200);
            Property(i => i.HashedPassword).IsRequired().HasMaxLength(200);
            Property(i => i.Salt).IsRequired().HasMaxLength(200);
            Property(i => i.IsLocked).IsRequired();
            Property(i => i.DateCreated);

            Property(i => i.LastChangeUserId).IsRequired();
            Property(i => i.CreationTimeStamp).IsRequired();
            Property(i => i.LastChangeTimeStamp).IsRequired();

            //Property(i => i.FirstName).IsRequired().HasMaxLength(100);
            //Property(i => i.LastName).IsRequired().HasMaxLength(100);
            //Property(i => i.IdentityCard).IsRequired().HasMaxLength(50);
            //Property(i => i.UniqueKey).IsRequired();
            //Property(i => i.Mobile).HasMaxLength(10);
            //Property(i => i.DateOfBirth).IsRequired();
        }
    }
}
