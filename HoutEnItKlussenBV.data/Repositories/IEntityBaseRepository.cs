﻿namespace HoutEnItKlussenBV.Data.Repositories
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using Entities;

    public interface IEntityBaseRepository<T> where T : class, IEntityBase, new()
    {
        IQueryable<T> All { get; }

        T GetSingle(int id);

        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);

        IQueryable<T> GetAll();

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        void Add(T entity);

        void Delete(T entity);

        void Edit(T entity);
    }
}
