﻿namespace HoutEnItKlussenBV.Data.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Linq.Expressions;
    using Entities;
    using Infrastructure;
    using Data;

    public class EntityBaseRepository<T> : IEntityBaseRepository<T>
            where T : class, IEntityBase, new()
    {
        private HoutEnItKlussenBVContext HoutEnItKlussenBVCtx;

        public EntityBaseRepository(IDbFactory databaseFactory)
        {
            DbFactory = databaseFactory;
        }

        public T GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().Where(predicate);
        }

        public virtual IQueryable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public virtual IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = DbContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected HoutEnItKlussenBVContext DbContext
        {
            get { return HoutEnItKlussenBVCtx ?? (HoutEnItKlussenBVCtx = DbFactory.Init()); }
        }

        public virtual IQueryable<T> All
        {
            get
            {
                return GetAll();
            }
        }

        public virtual void Add(T entity)
        {
            DbEntityEntry databaseEntityEntry = DbContext.Entry(entity);
            DbContext.Set<T>().Add(entity);
        }

        public virtual void Edit(T entity)
        {
            DbEntityEntry databaseEntityEntry = DbContext.Entry(entity);
            databaseEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry databaseEntityEntry = DbContext.Entry(entity);
            databaseEntityEntry.State = EntityState.Deleted;
        }
    }
}
