namespace HoutEnItKlussenBV.Data
{

    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Configurations;
    using Entities;

    public class HoutEnItKlussenBVContext : DbContext
    {
        public HoutEnItKlussenBVContext() : base("name=HoutEnItKlussenBVContext")
        {
            Database.SetInitializer<HoutEnItKlussenBVContext>(null);
        }

        public IDbSet<Error> ErrorSet { get; set; }

        #region Entity Sets
        public IDbSet<Address> AddressSet { get; set; }

        public IDbSet<Barcode> BarcodeSet { get; set; }

        public IDbSet<Customer> CustomerSet { get; set; }

        public IDbSet<Image> ImageSet { get; set; }

        public IDbSet<Part> PartSet { get; set; }

        public IDbSet<PartType> PartTypeSet { get; set; }

        public IDbSet<Project> ProjectSet { get; set; }

        public IDbSet<ProjectTask> ProjectTaskSet { get; set; }

        public IDbSet<Stock> StockSet { get; set; }

        public IDbSet<StockType> StockTypeSet { get; set; }

        public IDbSet<Task> TaskSet { get; set; }

        public IDbSet<TaskImage> TaskImageSet { get; set; }

        public IDbSet<TaskPart> TaskPartSet { get; set; }

        public IDbSet<TaskType> TaskTypeSet { get; set; }
        #endregion

        #region Membership Entity sets
        public IDbSet<Role> RoleSet { get; set; }

        public IDbSet<User> UserSet { get; set; }

        public IDbSet<UserRole> UserRoleSet { get; set; }
        #endregion

        public virtual void Commit()
        {
            //base.SaveChanges();
            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Configurations.Add(new ErrorConfiguration());

            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());

            modelBuilder.Configurations.Add(new AddressConfiguration());
            modelBuilder.Configurations.Add(new CustomerConfiguration());

            modelBuilder.Configurations.Add(new PartTypeConfiguration());
            modelBuilder.Configurations.Add(new PartConfiguration());
            modelBuilder.Configurations.Add(new ImageConfiguration());
            modelBuilder.Configurations.Add(new BarcodeConfiguration());
            modelBuilder.Configurations.Add(new StockTypeConfiguration());
            modelBuilder.Configurations.Add(new StockConfiguration());
            modelBuilder.Configurations.Add(new ProjectConfiguration());
            modelBuilder.Configurations.Add(new TaskTypeConfiguration());
            modelBuilder.Configurations.Add(new TaskConfiguration());
            modelBuilder.Configurations.Add(new TaskImageConfiguration());
            modelBuilder.Configurations.Add(new TaskPartConfiguration());
            modelBuilder.Configurations.Add(new ProjectTaskConfiguration());
        }
    }
}
