﻿//namespace HoutEnItKlussenBV.Data.Migrations
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Data.Entity.Migrations;
//    using Entities;

//    public sealed class Configuration : DbMigrationsConfiguration<HoutEnItKlussenBVContext>
//    {

//        private DateTime DateTimeNow = DateTime.Now;
//        EntityBaseColumns baseColumns = new EntityBaseColumns();

//        public Configuration()
//        {
//            AutomaticMigrationsEnabled = true;
//            AutomaticMigrationDataLossAllowed = true;
//        }

//        protected override void Seed(HoutEnItKlussenBVContext context)
//        {
//            #region Address
//            //Debugger();
//            List<int> addressIds = new List<int>();
//            foreach (var address in GenerateAddresses())
//            {
//                if (!addressIds.Contains(address.Id))
//                {
//                    addressIds.Add(address.Id);
//                }
//            }
//            if (context.AddressSet.Find(addressIds[0]) == null)
//            {
//                context.AddressSet.AddOrUpdate(i => i.Id, GenerateAddresses());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Customer
//            //Debugger();
//            List<int> customerIds = new List<int>();
//            foreach (var customer in GenerateCustomers())
//            {
//                if (!customerIds.Contains(customer.Id))
//                {
//                    customerIds.Add(customer.Id);
//                }
//            }
//            if (context.CustomerSet.Find(customerIds[0]) == null)
//            {
//                context.CustomerSet.AddOrUpdate(i => i.Id, GenerateCustomers());
//                context.SaveChanges();
//            }
//            #endregion
//            #region PartType
//            //Debugger();
//            List<int> parttypeIds = new List<int>();
//            foreach (var parttype in GeneratePartTypes())
//            {
//                if (!parttypeIds.Contains(parttype.Id))
//                {
//                    parttypeIds.Add(parttype.Id);
//                }
//            }
//            if (context.PartTypeSet.Find(parttypeIds[0]) == null)
//            {
//                context.PartTypeSet.AddOrUpdate(i => i.Id, GeneratePartTypes());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Part
//            //Debugger();
//            List<int> partIds = new List<int>();
//            foreach (var part in GenerateParts())
//            {
//                if (!partIds.Contains(part.Id))
//                {
//                    partIds.Add(part.Id);
//                }
//            }
//            if (context.PartSet.Find(partIds[0]) == null)
//            {
//                context.PartSet.AddOrUpdate(i => i.Id, GenerateParts());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Image
//            //Debugger();
//            List<int> imageIds = new List<int>();
//            foreach (var image in GenerateImages())
//            {
//                if (!imageIds.Contains(image.Id))
//                {
//                    imageIds.Add(image.Id);
//                }
//            }
//            if (context.ImageSet.Find(imageIds[0]) == null)
//            {
//                context.ImageSet.AddOrUpdate(i => i.Id, GenerateImages());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Barcode
//            //Debugger();
//            List<int> barcodeIds = new List<int>();
//            foreach (var barcode in GenerateBarcodes())
//            {
//                if (!barcodeIds.Contains(barcode.Id))
//                {
//                    barcodeIds.Add(barcode.Id);
//                }
//            }
//            if (context.BarcodeSet.Find(barcodeIds[0]) == null)
//            {
//                context.BarcodeSet.AddOrUpdate(i => i.Id, GenerateBarcodes());
//                context.SaveChanges();
//            }
//            #endregion
//            #region StockType
//            //Debugger();
//            List<int> stocktypeIds = new List<int>();
//            foreach (var stocktype in GenerateStockTypes())
//            {
//                if (!stocktypeIds.Contains(stocktype.Id))
//                {
//                    stocktypeIds.Add(stocktype.Id);
//                }
//            }
//            if (context.StockTypeSet.Find(stocktypeIds[0]) == null)
//            {
//                context.StockTypeSet.AddOrUpdate(i => i.Id, GenerateStockTypes());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Stock
//            //Debugger();
//            List<int> stockIds = new List<int>();
//            foreach (var stocktype in GenerateStocks())
//            {
//                if (!stockIds.Contains(stocktype.Id))
//                {
//                    stockIds.Add(stocktype.Id);
//                }
//            }
//            if (context.StockSet.Find(stocktypeIds[0]) == null)
//            {
//                context.StockSet.AddOrUpdate(i => i.Id, GenerateStocks());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Project
//            //Debugger();
//            List<int> projectIds = new List<int>();
//            foreach (var project in GenerateProjects())
//            {
//                if (!projectIds.Contains(project.Id))
//                {
//                    projectIds.Add(project.Id);
//                }
//            }
//            if (context.ProjectSet.Find(projectIds[0]) == null)
//            {
//                context.ProjectSet.AddOrUpdate(i => i.Id, GenerateProjects());
//                context.SaveChanges();
//            }
//            #endregion
//            #region TaskType
//            //Debugger();
//            List<int> tasktypeIds = new List<int>();
//            foreach (var tasktype in GenerateTaskTypes())
//            {
//                if (!tasktypeIds.Contains(tasktype.Id))
//                {
//                    tasktypeIds.Add(tasktype.Id);
//                }
//            }
//            if (context.TaskTypeSet.Find(tasktypeIds[0]) == null)
//            {
//                context.TaskTypeSet.AddOrUpdate(i => i.Id, GenerateTaskTypes());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Task
//            //Debugger();
//            List<int> taskIds = new List<int>();
//            foreach (var task in GenerateTasks())
//            {
//                if (!taskIds.Contains(task.Id))
//                {
//                    taskIds.Add(task.Id);
//                }
//            }
//            if (context.TaskSet.Find(taskIds[0]) == null)
//            {
//                context.TaskSet.AddOrUpdate(i => i.Id, GenerateTasks());
//                context.SaveChanges();
//            }
//            #endregion
//            #region TaskPart
//            //Debugger();
//            List<int> taskpartIds = new List<int>();
//            foreach (var taskpart in GenerateTaskParts())
//            {
//                if (!taskpartIds.Contains(taskpart.Id))
//                {
//                    taskpartIds.Add(taskpart.Id);
//                }
//            }
//            if (context.TaskPartSet.Find(taskpartIds[0]) == null)
//            {
//                context.TaskPartSet.AddOrUpdate(i => i.Id, GenerateTaskParts());
//                context.SaveChanges();
//            }
//            #endregion
//            #region TaskImage
//            Debugger();
//            List<int> taskimageIds = new List<int>();
//            foreach (var taskimage in GenerateTaskImages())
//            {
//                if (!taskimageIds.Contains(taskimage.Id))
//                {
//                    taskimageIds.Add(taskimage.Id);
//                }
//            }
//            if (context.TaskImageSet.Find(taskimageIds[0]) == null)
//            {
//                context.TaskImageSet.AddOrUpdate(i => i.Id, GenerateTaskImages());
//                context.SaveChanges();
//            }
//            #endregion
//            #region ProjectTask
//            //Debugger();
//            List<int> projecttaskIds = new List<int>();
//            foreach (var projecttask in GenerateProjectTasks())
//            {
//                if (!projecttaskIds.Contains(projecttask.Id))
//                {
//                    projecttaskIds.Add(projecttask.Id);
//                }
//            }
//            if (context.ProjectTaskSet.Find(projecttaskIds[0]) == null)
//            {
//                context.ProjectTaskSet.AddOrUpdate(i => i.Id, GenerateProjectTasks());
//                context.SaveChanges();
//            }
//            #endregion
//            #region Membership
//            //Debugger();
//            List<int> roleIds = new List<int>();
//            foreach (var role in GenerateRoles())
//            {
//                if (!roleIds.Contains(role.Id))
//                {
//                    roleIds.Add(role.Id);
//                }
//            }
//            if (context.RoleSet.Find(roleIds[0]) == null)
//            {
//                context.RoleSet.AddOrUpdate(i => i.Name, GenerateRoles());
//                context.SaveChanges();
//            }

//            //Debugger();
//            List<int> userIds = new List<int>();
//            foreach (var user in GenerateUsers())
//            {
//                if (!userIds.Contains(user.Id))
//                {
//                    userIds.Add(user.Id);
//                }
//            }
//            if (context.UserSet.Find(userIds[0]) == null)
//            {
//                context.UserSet.AddOrUpdate(i => i.Id, GenerateUsers());
//                context.SaveChanges();
//            }

//            //Debugger();
//            List<int> userroleIds = new List<int>();
//            foreach (var userrole in GenerateUserRoles())
//            {
//                if (!userroleIds.Contains(userrole.Id))
//                {
//                    userroleIds.Add(userrole.Id);
//                }
//            }
//            if (context.UserRoleSet.Find(userroleIds[0]) == null)
//            {
//                context.UserRoleSet.AddOrUpdate(i => i.Id, GenerateUserRoles());
//                context.SaveChanges();
//            }
//            #endregion
//        }

//        private Address[] GenerateAddresses()
//        {
//            Address[] addresses = new Address[] {
//                // Id = 1
//                new Address()
//                { Id = 1,
//                    Street = "Overflakkee",
//                    Number = "20",
//                    NumberExt = null,
//                    Zipcode = "8302NX",
//                    Town = "Emmeloord",
//                    Province = "Flevoland",
//                    Country = "Nederland",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                // Id = 2
//                new Address()
//                { Id = 2,
//                    Street = "Baan",
//                    Number = "2",
//                    NumberExt = "c",
//                    Zipcode = "1234AB",
//                    Town = "IJsselmuiden",
//                    Province = "Overijssel",
//                    Country = "Nederland",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return addresses;
//        }

//        private Customer[] GenerateCustomers()
//        {
//            Customer[] customers = new Customer[] {
//                new Customer()
//                { Id = 1,
//                    FirstName = "Eltjo",
//                    LastName = "Kooi",
//                    Email = "loe.lap@home.nl",
//                    UniqueKey = Guid.NewGuid(),
//                    DateOfBirth = new DateTime(1957,3,16),
//                    Mobile ="0630956360",
//                    Phone = "0527619957",
//                    RegistrationDate = baseColumns.CreationTimeStamp,
//                    AddressId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Customer()
//                { Id = 2,
//                    FirstName = "Ulfert",
//                    LastName = "van der Kamp",
//                    Email = "ullie@kampo.nl",
//                    UniqueKey = Guid.NewGuid(),
//                    DateOfBirth = new DateTime(1965,7,25),
//                    Mobile ="0630956360",
//                    Phone = "0382533352",
//                    RegistrationDate = baseColumns.CreationTimeStamp,
//                    AddressId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return customers;
//        }

//        private PartType[] GeneratePartTypes()
//        {
//            PartType[] parttypes = new PartType[] {
//                new PartType()
//                { Id = 1,
//                    Name = "Spijkers",
//                    Description = "Bevestigingsmaterialen",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new PartType()
//                { Id = 2,
//                    Name = "GipsSchroeven",
//                    Description = "Bevestigingsmaterialen",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new PartType()
//                { Id = 3,
//                    Name = "Houtschroeven",
//                    Description = "Bevestigingsmaterialen",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return parttypes;
//        }

//        private Part[] GenerateParts()
//        {
//            Part[] parts = new Part[] {
//                new Part()
//                { Id = 1,
//                    Name = "draadnagel 100mm",
//                    Description = "Staal",
//                    ImageId = 4,        //"spijker_100mm.jpg",
//                    PartTypeId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Part()
//                { Id = 2,
//                    Name = "Draadnagel 50mm",
//                    Description = "staal",
//                    ImageId = 5,        //"Spijker_50mm.jpg",
//                    PartTypeId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Part()
//                { Id = 3,
//                    Name = "Gipsschroef 40mm",
//                    Description = "Gips",
//                    ImageId = 6,   //"Gispschroef_40mm.jpg",
//                    PartTypeId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Part()
//                { Id = 4,
//                    Name = "Gipsschroef 20mm",
//                    Description = "Gips",
//                    ImageId = 7,    //"Gispschroef_20mm.jpg",
//                    PartTypeId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Part()
//                { Id = 5,
//                    Name = "Houtschroef 30mmx3,5mm",
//                    Description = "Hout",
//                    ImageId = 8,    //  "Houtschroef_30mmx3,5mm.jpg",
//                    PartTypeId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Part()
//                { Id = 6,
//                    Name = "Houtschroef 35mmx4,0mm",
//                    Description = "Hout",
//                    ImageId = 9,        // "Houtschroef_35mmx4,0mm.jpg",
//                    PartTypeId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return parts;
//        }

//        private Image[] GenerateImages()
//        {
//            var locationContentImages = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\";

//            Image[] images = new Image[] {
//                new Image()
//                { Id = 1,
//                    Name = "Task1-1.jpg",
//                    Description = "Hout",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 2,
//                    Name = "Task1-2.jpg",
//                    Description = "Hout",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 3,
//                    Name = "Task1-3.jpg",
//                    Description = "Hout",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 4,
//                    Name = "spijker_100mm.jpg",
//                    Description = "Staal, draadnagel 100mm",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 5,
//                    Name = "Spijker_50mm.jpg",
//                    Description = "Staal, Draadnagel 50mm",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 6,
//                    Name = "Gispschroef_40mm.jpg",
//                    Description = "Gips, Gipsschroef 40mm",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 7,
//                    Name = "Gispschroef_20mm.jpg",
//                    Description = "Gips, Gipsschroef 20mm",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 8,
//                    Name = "Houtschroef_30mmx3,5mm.jpg",
//                    Description = "Hout, Houtschroef 30mmx3,5mm",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 9,
//                    Name = "Houtschroef_35mmx4,0mm.jpg",
//                    Description = "Hout, Houtschroef 35mmx4,0mm",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    LocationUrl = locationContentImages,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 10,
//                    Name = "Task2-1.jpg",
//                    Description = "Schakelmateriaal 1",
//                    LocationUrl = locationContentImages,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 11,
//                    Name = "Task2-2.jpg",
//                    Description = "Schakelmateriaal 2",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 12,
//                    Name = "Task2-3.jpg",
//                    Description = "Schakelmateriaal 3",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 13,
//                    Name = "Task3-1.jpg",
//                    Description = "Electriciteit",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 14,
//                    Name = "Task3-2.jpg",
//                    Description = "Electriciteit",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 15,
//                    Name = "Task3-3.jpg",
//                    Description = "Electriciteit",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 16,
//                    Name = "Task4-1.jpg",
//                    Description = "Trap 1",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 17,
//                    Name = "Task4-2.jpg",
//                    Description = "Trap 2",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Image()
//                { Id = 18,
//                    Name = "Task4-3.jpg",
//                    Description = "Trap 3",
//                    LocationUrl = @"D:\Home Projects\Provisionals\HoutEnItKlussenBV\HoutEnItKlussenBV.Web\Content\images\",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return images;
//        }

//        private Barcode[] GenerateBarcodes()
//        {
//            Barcode[] parts = new Barcode[] {
//                new Barcode()
//                { Id = 1,
//                    PartId = 1,
//                    Code = "",
//                    CodeName = "",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Barcode()
//                { Id = 2,
//                    PartId = 2,
//                    Code = "",
//                    CodeName = "",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Barcode()
//                { Id = 3,
//                    PartId = 3,
//                    Code = "",
//                    CodeName = "",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Barcode()
//                { Id = 4,
//                    PartId = 4,
//                    Code = "",
//                    CodeName = "",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Barcode()
//                { Id = 5,
//                    PartId = 5,
//                    Code = "",
//                    CodeName = "",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Barcode()
//                { Id = 6,
//                    PartId = 6,
//                    Code = "",
//                    CodeName = "",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return parts;
//        }

//        private StockType[] GenerateStockTypes()
//        {
//            StockType[] stocktypes = new StockType[] {
//                new StockType()
//                { Id = 1,
//                    IsSingleStockItem = true,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new StockType()
//                { Id = 2,
//                    IsSingleStockItem = true,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new StockType()
//                { Id = 3,
//                    IsSingleStockItem = true,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return stocktypes;
//        }

//        private Stock[] GenerateStocks()
//        {
//            Stock[] stocks = new Stock[] {
//                new Stock()
//                { Id = 1,
//                    PartId = 1,
//                    StockTypeId = 1,
//                    NumberInStock = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Stock()
//                { Id = 2,
//                    PartId = 2,
//                    StockTypeId = 2,
//                    NumberInStock = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Stock()
//                { Id = 3,
//                    PartId = 3,
//                    StockTypeId = 3,
//                    NumberInStock = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Stock()
//                { Id = 4,
//                    PartId = 4,
//                    StockTypeId = 1,
//                    NumberInStock = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Stock()
//                { Id = 5,
//                    PartId = 5,
//                    StockTypeId = 2,
//                    NumberInStock = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Stock()
//                { Id = 6,
//                    PartId = 6,
//                    StockTypeId = 3,
//                    NumberInStock = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return stocks;
//        }

//        private Project[] GenerateProjects()
//        {
//            Project[] projects = new Project[] {
//                new Project()
//                { Id = 1,
//                    Name = "Project 1",
//                    Description = "Project 1",
//                    CustomerId = 1,
//                    DateCreated = baseColumns.CreationTimeStamp,
//                    DateReady = baseColumns.CreationTimeStamp.AddMonths(2),
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Project()
//                { Id = 2,
//                    Name = "Project 2",
//                    Description = "Project 2",
//                    CustomerId = 1,
//                    DateCreated = baseColumns.CreationTimeStamp,
//                    DateReady = baseColumns.CreationTimeStamp.AddMonths(2),
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Project()
//                { Id = 3,
//                    Name = "Project 3",
//                    Description = "Project 3",
//                    CustomerId = 1,
//                    DateCreated = baseColumns.CreationTimeStamp,
//                    DateReady = baseColumns.CreationTimeStamp.AddMonths(2),
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Project()
//                { Id = 4,
//                    Name = "Project 4",
//                    Description = "Project 4",
//                    CustomerId = 2,
//                    DateCreated = baseColumns.CreationTimeStamp,
//                    DateReady = baseColumns.CreationTimeStamp.AddMonths(2),
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Project()
//                { Id = 5,
//                    Name = "Project 5",
//                    Description = "Project 5",
//                    CustomerId = 2,
//                    DateCreated = baseColumns.CreationTimeStamp,
//                    DateReady = baseColumns.CreationTimeStamp,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp.AddMonths(2),
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Project()
//                { Id = 6,
//                    Name = "Project 6",
//                    Description = "Project 6",
//                    CustomerId = 2,
//                    DateCreated = baseColumns.CreationTimeStamp,
//                    DateReady = baseColumns.CreationTimeStamp.AddMonths(2),
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return projects;
//        }

//        private TaskType[] GenerateTaskTypes()
//        {
//            TaskType[] tasktypes = new TaskType[] {
//                new TaskType()
//                { Id = 1,
//                    Name = "TaskType 1",
//                    Description = "Omschrijving TaskType 1",
//                    //Image = "TaskType1.jpg",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskType()
//                { Id = 2,
//                    Name = "TaskType 2",
//                    Description = "Omschrijving TaskType 2",
//                    //Image = "TaskType2.jpg",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskType()
//                { Id = 3,
//                    Name = "TaskType 3",
//                    Description = "Omschrijving TaskType 3",
//                    //Image = "TaskType3.jpg",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskType()
//                { Id = 4,
//                    Name = "TaskType 4",
//                    Description = "Omschrijving TaskType 4",
//                    //Image = "TaskType4.jpg",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return tasktypes;
//        }

//        private Task[] GenerateTasks()
//        {
//            Task[] tasks = new Task[] {
//                new Task()
//                { Id = 1,
//                    Name = "Task 1",
//                    Description = "Task 1",
//                    TaskTypeId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Task()
//                { Id = 2,
//                    Name = "Task 2",
//                    Description = "Task 2",
//                    TaskTypeId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Task()
//                { Id = 3,
//                    Name = "Task 3",
//                    Description = "Task 3",
//                    TaskTypeId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Task()
//                { Id = 4,
//                    Name = "Task 4",
//                    Description = "Task 4",
//                    TaskTypeId = 4,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return tasks;
//        }

//        private TaskPart[] GenerateTaskParts()
//        {
//            TaskPart[] taskparts = new TaskPart[] {
//                new TaskPart()
//                { Id = 1,
//                    TaskId = 1,
//                    PartId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 2,
//                    TaskId = 1,
//                    PartId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 3,
//                    TaskId = 1,
//                    PartId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                // Id = 4
//                new TaskPart()
//                { Id = 4,
//                    TaskId = 1,
//                    PartId = 4,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 5,
//                    TaskId = 1,
//                    PartId = 5,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 6,
//                    TaskId = 1,
//                    PartId = 6,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                // Id = 7
//                new TaskPart()
//                { Id = 7,
//                    TaskId = 2,
//                    PartId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 8,
//                    TaskId = 2,
//                    PartId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 9,
//                    TaskId = 2,
//                    PartId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 10,
//                    TaskId = 2,
//                    PartId = 4,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 11,
//                    TaskId = 2,
//                    PartId = 5,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 12,
//                    TaskId = 2,
//                    PartId = 6,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 13,
//                    TaskId = 3,
//                    PartId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 14,
//                    TaskId = 3,
//                    PartId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 15,
//                    TaskId = 3,
//                    PartId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 16,
//                    TaskId = 3,
//                    PartId = 4,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 17,
//                    TaskId = 3,
//                    PartId = 5,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskPart()
//                { Id = 18,
//                    TaskId = 3,
//                    PartId = 6,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return taskparts;
//        }

//        /// <summary>
//        ///  Busines Rule max 3 images per task
//        /// </summary>
//        /// <returns></returns>
//        private TaskImage[] GenerateTaskImages()
//        {
//            TaskImage[] taskimages = new TaskImage[] {
//                new TaskImage()
//                { Id = 1,
//                    TaskId = 1,
//                    ImageId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 2,
//                    TaskId = 1,
//                    ImageId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 3,
//                    TaskId = 1,
//                    ImageId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 4,
//                    TaskId = 2,
//                    ImageId = 10,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 5,
//                    TaskId = 2,
//                    ImageId = 11,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 6,
//                    TaskId = 2,
//                    ImageId = 12,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 7,
//                    TaskId = 3,
//                    ImageId = 13,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 8,
//                    TaskId = 3,
//                    ImageId = 14,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 9,
//                    TaskId = 3,
//                    ImageId = 15,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 10,
//                    TaskId = 4,
//                    ImageId = 16,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 11,
//                    TaskId = 4,
//                    ImageId = 17,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new TaskImage()
//                { Id = 12,
//                    TaskId = 4,
//                    ImageId = 18,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return taskimages;
//        }

//        private ProjectTask[] GenerateProjectTasks()
//        {
//            ProjectTask[] projecttasks = new ProjectTask[] {
//                new ProjectTask()
//                { Id = 1,
//                    ProjectId = 1,
//                    TaskId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new ProjectTask()
//                { Id = 2,
//                    ProjectId = 2,
//                    TaskId = 1,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new ProjectTask()
//                { Id = 3,
//                    ProjectId = 3,
//                    TaskId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new ProjectTask()
//                { Id = 4,
//                    ProjectId = 4,
//                    TaskId = 2,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new ProjectTask()
//                { Id = 5,
//                    ProjectId = 5,
//                    TaskId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new ProjectTask()
//                { Id = 6,
//                    ProjectId = 6,
//                    TaskId = 3,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return projecttasks;
//        }

//        #region Membership
//        private Role[] GenerateRoles()
//        {
//            Role[] roles = new Role[]{
//                new Role()
//                { Id = 1,
//                    Name = "SUPER_ADMIN" ,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Role()
//                { Id = 2,
//                    Name = "BASIC_ADMIN" ,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new Role()
//                { Id = 3,
//                    Name = "ELEVATED_USER",
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp  },
//                new Role()
//                { Id = 4,
//                    Name = "BASIC_USER" ,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//            };

//            return roles;
//        }

//        private User[] GenerateUsers()
//        {
//            User[] users = new User[] {
//                new User()
//                { Id = 1,
//                    Email = "info@van-der-kamp.nl",
//                    Username = "KWHJ",
//                    HashedPassword = "DCSzXo2ckLeB3tKckVZt7K+YtYW1RN6tAu++EMdq7WY=",
//                    Salt = "txzhYDuClNHH8B3Lr42NOg==",
//                    IsLocked = false,
//                    DateCreated = DateTime.Now,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new User()
//                { Id = 2,
//                    Email = "eltjo@kooi.nl",
//                    Username = "EKOOI",
//                    HashedPassword = "",
//                    Salt = "",
//                    IsLocked = false,
//                    DateCreated = DateTime.Now,
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp }
//            };

//            return users;
//        }

//        private UserRole[] GenerateUserRoles()
//        {
//            UserRole[] userroles = new UserRole[] {
//                new UserRole()
//                { Id = 1,
//                    RoleId = 1,  // SUPER_ADMIN
//                    UserId = 1,  // KWHJ
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp },
//                new UserRole()
//                { Id = 2,
//                    RoleId = 2,  // BASIC_ADMIN
//                    UserId = 2,  // Eltjo
//                    LastChangeUserId = baseColumns.LastChangeUserId,
//                    CreationTimeStamp = baseColumns.CreationTimeStamp,
//                    LastChangeTimeStamp = baseColumns.LastChangeTimeStamp
//                }
//            };

//            return userroles;
//        }
//        #endregion

//        private class EntityBaseColumns
//        {
//            private int lastChangeUserId = 1;
//            public int LastChangeUserId { get { return lastChangeUserId; } }

//            private DateTime creationTimeStamp = DateTime.Now;
//            public DateTime CreationTimeStamp { get { return creationTimeStamp; } }

//            private DateTime lastChangeTimeStamp = DateTime.Now;
//            public DateTime LastChangeTimeStamp { get { return lastChangeTimeStamp; } }

//        }

//        /// <summary>
//        /// Uncomment the line Debugger just before the expected spot of problem
//        /// </summary>
//        private void Debugger()
//        {
//            if (System.Diagnostics.Debugger.IsAttached == false)
//                System.Diagnostics.Debugger.Launch();
//        }
//    }
//}
