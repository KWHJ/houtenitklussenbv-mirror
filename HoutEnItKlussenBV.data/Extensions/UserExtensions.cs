﻿namespace HoutEnItKlussenBV.Data.Extensions
{
    using Entities;
    using Repositories;
    using System.Linq;

    public static class UserExtensions
    {
        public static bool UserExists(this IEntityBaseRepository<User> usersRepository, string email
            //, string identityCard
            )
        {
            bool userExists = false;

            userExists = usersRepository
                .GetAll()
                .Any(c => c.Email.ToLower() == email
                    //|| c.IdentityCard.ToLower() == identityCard
                    );

            return userExists;
        }

        public static User GetSingleByUsername(this IEntityBaseRepository<User> userRepository, string username)
        {
            var bla = userRepository.GetAll().FirstOrDefault(x => x.Username == username);
            return userRepository.GetAll().FirstOrDefault(x => x.Username == username);
        }
    }
}