namespace HoutEnItKlussenBV.Entities
{
    using System;

    public class UserRole : IEntityBase
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int RoleId { get; set; }

        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}
