namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class User : IEntityBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class
        /// </summary>
        public User()
        {
            UserRoles = new List<UserRole>();
        }

        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string HashedPassword { get; set; }

        public string Salt { get; set; }

        public bool IsLocked { get; set; }

        public DateTime DateCreated { get; set; }

        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
