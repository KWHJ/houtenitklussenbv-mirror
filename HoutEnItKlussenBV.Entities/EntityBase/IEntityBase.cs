namespace HoutEnItKlussenBV.Entities
{
    public interface IEntityBase
    {
        /// <summary>
        /// Gets or sets ID of IEntityBase4
        /// </summary>
        int Id{ get; set; }
    }
}
