namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class StockType : IEntityBase
    {
        public int Id { get; set; }

        public bool IsSingleStockItem { get; set; }

        public virtual ICollection<Stock> Stocks { get; set; }

        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
