namespace HoutEnItKlussenBV.Entities
{
    using System;

    public class Stock : IEntityBase
    {
        public int Id { get; set; }

        public int PartId { get; set; }

        public bool IsAvailable { get; set; }

        public int StockTypeId { get; set; }

        public int? NumberInStock { get; set; }

        public virtual Part Part { get; set; }

        public virtual StockType StockType { get; set; }

        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
