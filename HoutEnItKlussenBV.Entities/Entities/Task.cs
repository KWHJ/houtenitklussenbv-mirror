namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class Task : IEntityBase
    {
        public Task() { }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int TaskTypeId { get; set; }

        public virtual TaskType TaskType { get; set; }

        public virtual ICollection<TaskImage> TaskImages { get; set; }

        public virtual ICollection<Image> Images { get; set; }

        public virtual Dictionary<int, Dictionary<string, string>> ImageNames { get; set; }

        public virtual ICollection<Project> Projects { get; set; }

        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
