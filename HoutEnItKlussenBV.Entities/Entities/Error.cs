namespace HoutEnItKlussenBV.Entities
{
    using System;

    public partial class Error : IEntityBase
    {
        public int Id { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
