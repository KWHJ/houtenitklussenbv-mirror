namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class Image : IEntityBase
    {
        /// <summary>
        /// NOTE: Switchin ON: generates error:
        /// The type 'ISet<>' is defined in an assembly that is not referenced.
        /// You must add a reference to assembly 'System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
        /// </summary>
        //public Image()
        //{
        //    Tasks = new HashSet<Task>();
        //}

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string LocationUrl { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }


        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
