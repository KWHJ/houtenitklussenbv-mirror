namespace HoutEnItKlussenBV.Entities
{
    using System;

    public class TaskPart : IEntityBase
    {
        public int Id { get; set; }

        public int TaskId { get; set; }

        public int PartId { get; set; }

        public virtual Task Task { get; set; }

        public virtual Part Part { get; set; }

        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
