namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class Customer : IEntityBase
    {

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public Guid UniqueKey { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Mobile { get; set; }

        public string Phone { get; set; }

        public DateTime RegistrationDate { get; set; }

        public int AddressId { get; set; }

        public int LastChangeUserId { get; set; }

        public virtual ICollection<Project> Projects { get; set; }

        #region "Base Properties"
        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }

        public virtual Address Address { get; set; }
        #endregion "Base Properties"

    }
}
