namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class Barcode : IEntityBase
    {
        public int Id { get; set; }

        public int PartId { get; set; }

        public string Code { get; set; }

        public string CodeName { get; set; }

        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
