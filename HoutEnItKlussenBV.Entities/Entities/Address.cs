namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class Address : IEntityBase
    {
        public int Id { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }

        public string NumberExt { get; set; }

        public string Zipcode { get; set; }

        public string Town { get; set; }

        public string Province { get; set; }

        public string Country { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }

        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
