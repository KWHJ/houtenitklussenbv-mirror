namespace HoutEnItKlussenBV.Entities
{
    using System;
    using System.Collections.Generic;

    public class Project : IEntityBase
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CustomerId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateReady { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual ICollection<ProjectTask> ProjectTasks { get; set; }

        #region "Base Properties"
        public int LastChangeUserId { get; set; }

        public DateTime CreationTimeStamp { get; set; }

        public DateTime LastChangeTimeStamp { get; set; }
        #endregion "Base Properties"
    }
}
