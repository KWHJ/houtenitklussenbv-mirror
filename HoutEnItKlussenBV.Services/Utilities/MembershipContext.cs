﻿namespace HoutEnItKlussenBV.Services.Utilities
{
    using System.Security.Principal;
    using Entities;

    public class MembershipContext
    {
        /// <summary>
        /// Gets or sets Principal
        /// </summary>
        public IPrincipal Principal { get; set; }

        /// <summary>
        /// Gets or sets User of type User
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is enabled
        /// </summary>
        public bool IsValid()
        {
            return Principal != null;
        }
    }
}
